package com.notebook.service.ChapterInfoService;


import com.notebook.entity.pojo.ChapterInfo;

import java.util.List;

public interface IChapterInfoService {
    List<ChapterInfo> findQueryAll(Integer videoId);
    Integer insert(ChapterInfo record);
    Integer deleteByPrimaryKey(Integer videoId);
    Integer deleteById(Integer chapterId);
}
