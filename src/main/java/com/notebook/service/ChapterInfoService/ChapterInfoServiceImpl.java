package com.notebook.service.ChapterInfoService;


import com.notebook.dao.IChapterInfoMapper;
import com.notebook.entity.pojo.ChapterInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ChapterInfoServiceImpl implements IChapterInfoService{
    @Autowired
    private IChapterInfoMapper chapterInfoMapper;

    @Override
    public List<ChapterInfo> findQueryAll(Integer videoId) {
        return chapterInfoMapper.findQueryAll(videoId);
    }

    @Override
    public Integer insert(ChapterInfo record) {
        return chapterInfoMapper.insert(record);
    }

    @Override
    public Integer deleteByPrimaryKey(Integer videoId) {
        return chapterInfoMapper.deleteByPrimaryKey(videoId);
    }

    @Override
    public Integer deleteById(Integer chapterId) {
        return chapterInfoMapper.deleteById(chapterId);
    }
}
