package com.notebook.service.CorporateService;

import com.notebook.dao.ICorporateMapper;
import com.notebook.entity.pojo.Corporate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CorporateServiceImpl implements ICorporateService {

    @Autowired
    private ICorporateMapper corporateMapper;
    @Override
    public Corporate selectByPrimaryKey(Integer corporateId) {
        return corporateMapper.selectByPrimaryKey(corporateId);
    }
}
