package com.notebook.service.CorporateService;

import com.notebook.entity.pojo.Corporate;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/6/8
 */
public interface ICorporateService {
    Corporate selectByPrimaryKey(Integer corporateId);
}
