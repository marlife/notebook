package com.notebook.service.RecruitService;

import com.notebook.entity.pojo.Recruit;
import com.notebook.entity.vo.recruit.BackRecruitVo;

import java.util.List;

public interface IRecruitService {

    List<BackRecruitVo> getBackRecruitInfo();
    Recruit selectByPrimaryKey(Integer recruitId);

}
