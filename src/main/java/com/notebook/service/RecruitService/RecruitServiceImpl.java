package com.notebook.service.RecruitService;

import com.notebook.dao.IRecruitMapper;
import com.notebook.entity.pojo.Recruit;
import com.notebook.entity.vo.recruit.BackRecruitVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecruitServiceImpl implements IRecruitService{

    @Autowired
    private IRecruitMapper recruitMapper;

    @Override
    public List<BackRecruitVo> getBackRecruitInfo() {
        return recruitMapper.getBackRecruitInfo();
    }

    @Override
    public Recruit selectByPrimaryKey(Integer recruitId) {
        return recruitMapper.selectByPrimaryKey(recruitId);
    }
}
