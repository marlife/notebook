package com.notebook.service.LuceneService;

import com.notebook.entity.pojo.News;
import com.notebook.entity.vo.news.LuceneNewsVo;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/10
 */
public interface ILuceneService {

    void createIndexing(String indexDir,List<LuceneNewsVo> list);

    void search(String indexDir,String title);

}
