package com.notebook.service.LuceneService;

import com.notebook.entity.vo.news.LuceneNewsVo;
import com.notebook.utils.LuceneUtil;
import org.springframework.expression.spel.ast.Indexer;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/10
 */
@Service
public class LuceneServiceImpl implements ILuceneService {
    @Override
    public void createIndexing(String indexDir,List<LuceneNewsVo> list){
        try {
            LuceneUtil.IndexWrite(indexDir,list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void search(String indexDir,String title){
        try {
            LuceneUtil.search(indexDir,title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
