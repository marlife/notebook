package com.notebook.service.MessageService;

import com.notebook.dao.IMessageDao;
import com.notebook.entity.pojo.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements IMessageService {

    @Autowired
    private IMessageDao messageDao;

    @Override
    public Integer addMessage(Message message) {
        return messageDao.addMessage(message);
    }
}
