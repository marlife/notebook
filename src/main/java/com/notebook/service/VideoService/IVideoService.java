package com.notebook.service.VideoService;

import com.notebook.entity.pojo.Video;
import com.notebook.entity.vo.video.VideoVo;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/6/1
 */
public interface IVideoService {

    Integer insertSelective(Video video);
    List<Video> queryAll();
    Integer deleteByPrimaryKey(Integer videoId);
    List<VideoVo> getTheLeastWatch();
    List<VideoVo> getTheMostWatch();
    List<Video> getTheLatestRelease();
}
