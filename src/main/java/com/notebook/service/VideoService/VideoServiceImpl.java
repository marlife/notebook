package com.notebook.service.VideoService;

import com.notebook.dao.IVideoMapper;
import com.notebook.entity.pojo.Video;
import com.notebook.entity.vo.video.VideoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoServiceImpl implements  IVideoService{

    @Autowired
    private IVideoMapper videoMapper;


    @Override
    public Integer insertSelective(Video video) {
        return videoMapper.insertSelective(video);
    }

    @Override
    public List<Video> queryAll() {
        return videoMapper.queryAll();
    }

    @Override
    public Integer deleteByPrimaryKey(Integer videoId) {
        return videoMapper.deleteByPrimaryKey(videoId);
    }

    @Override
    public List<VideoVo> getTheLeastWatch() {
        return videoMapper.getTheLeastWatch();
    }

    @Override
    public List<VideoVo> getTheMostWatch() {
        return videoMapper.getTheMostWatch();
    }

    @Override
    public List<Video> getTheLatestRelease() {
        return videoMapper.getTheLatestRelease();
    }
}
