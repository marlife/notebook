package com.notebook.service.NoteService;

import com.notebook.dao.INoteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/30
 */
@Service
public class NoteServiceImpl implements INoteService {
    @Autowired
    private INoteDao noteDaoMapper;
    @Override
    public int deleteUserByPrimaryKey(Integer userId) {
        return noteDaoMapper.deleteUserByPrimaryKey(userId);
    }
}
