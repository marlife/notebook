package com.notebook.service.NoteService;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/30
 */
public interface INoteService {
    //按用户id删除便签
    int deleteUserByPrimaryKey(Integer userId);
}
