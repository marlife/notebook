package com.notebook.service.ProposalService;

import com.notebook.entity.pojo.Proposal;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/31
 */
public interface IProposalService {
    //新增建议
    boolean insert(Proposal record);
    //查询全部建议
    List<Proposal> queryAll();

    //获取未读的信息数量
    Integer GetUnreadInfoCount();


    //根据id设置已读
    Integer settingRead(Integer id);

    //获取未读的留言内容
    List<Proposal> getNoReadList();
    //根据id删除留言
    Integer delById(Integer id);

}
