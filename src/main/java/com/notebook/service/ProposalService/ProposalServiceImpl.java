package com.notebook.service.ProposalService;

import com.notebook.dao.IProposalDao;
import com.notebook.entity.pojo.Proposal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/31
 */
@Service
public class ProposalServiceImpl implements IProposalService {

    @Autowired
    private IProposalDao proposalDaoMapper;
    @Override
    public boolean insert(Proposal record) {
        return proposalDaoMapper.insert(record)>0;
    }

    @Override
    public List<Proposal> queryAll() {

        return proposalDaoMapper.queryAll();
    }


    @Override
    public Integer GetUnreadInfoCount() {
        return proposalDaoMapper.GetUnreadInfoCount();
    }


    @Override
    public Integer settingRead(Integer id) {
        return proposalDaoMapper.settingRead(id);
    }

    @Override
    public List<Proposal> getNoReadList() {
        return proposalDaoMapper.getNoReadList();
    }

    @Override
    public Integer delById(Integer id) {
        return proposalDaoMapper.delById(id);
    }

}
