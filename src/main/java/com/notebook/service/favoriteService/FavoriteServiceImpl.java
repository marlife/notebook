package com.notebook.service.favoriteService;

import com.notebook.dao.IFavoriteDao;
import com.notebook.entity.pojo.Favorite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/17
 */
@Service
public class FavoriteServiceImpl implements IFavoriteService{
    @Autowired
    private IFavoriteDao favoriteDao;
    @Override
    public int deleteByPrimaryKey(Integer newsId) {
        return favoriteDao.deleteByPrimaryKey(newsId);
    }

    @Override
    public int deleteUserByPrimaryKey(Integer userId) {
        return favoriteDao.deleteUserByPrimaryKey(userId);
    }

    @Override
    public boolean insert(Favorite record) {
        return favoriteDao.insert(record)>0;
    }

    @Override
    public Integer getYesNoFavo(Favorite favorite) {
        return favoriteDao.getYesNoFavo(favorite);
    }

    @Override
    public Integer deleteById(Integer id) {
        return favoriteDao.deleteById(id);
    }

    @Override
    public Integer getFavoriteCount() {
        return favoriteDao.getFavoriteCount();
    }

    @Override
    public List<Integer> findByUserIdGetNewsId(Integer userId) {
        return favoriteDao.findByUserIdGetNewsId(userId);
    }
}
