package com.notebook.service.favoriteService;

import com.notebook.entity.pojo.Favorite;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/17
 */
public interface IFavoriteService {

    //按新闻id删除收藏
    int deleteByPrimaryKey(Integer newsId);
    //按用户id删除收藏
    int deleteUserByPrimaryKey(Integer userId);
    //设置收藏
    boolean insert(Favorite record);
    //查询这条新闻这个用户是否收藏
    Integer getYesNoFavo(Favorite favorite);
    //根据主键id删除此数据
    Integer deleteById(Integer id);
    //获取共累计收藏数量
    Integer getFavoriteCount();
    //根据用户id查询全部的新闻id
    List<Integer> findByUserIdGetNewsId(Integer userId);

}
