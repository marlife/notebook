package com.notebook.service.UserService;

import com.notebook.entity.pojo.News;
import com.notebook.entity.pojo.User;
import com.notebook.entity.vo.user.*;

import java.util.List;
import java.util.Map;

/**
 * 文件描述:  定义用户相关的业务操作的接口
 * 创建用户:emotion
 * 创建时间:2018/3/11
 */
public interface IUserService {


    //根据id查询用户信息
    User selectByPrimaryKey(Integer id);
    //查询所有用户
    List<User> userList();

    //按用户id删除用户
    boolean deleteByPrimaryKey(Integer userId);
    //用户登录
    List<LoginUserVO> UserLogin(LoginUserVO userVO);
    //用户注册  返回主键
    int UserReg(AddUserVO userVO);
    //检测手机号
    boolean inspectPhone(String phone);
    //用户找回密码
    boolean BackPassword(LoginUserVO userVO);
    //根据手机号查询用户信息
    User FindByPhone(String phone);
    //用户修改个人信息
    boolean UpdateFindByid(UpdateInfoVO record);
    //根据账号修改密码
    boolean updatePassoWord(UpdatePwdVO pwdVO);
    //查询所有管理員
    List<User> adminList();
    //添加管理员
    Integer addAdmin(AdminRegVo regVo);
    //修改信息
    boolean UpdateInfo(AdminRegVo regVo);
    //修改用户头像
    Integer updateUserPic(String pic, Integer userId);
    //获取用户显示的信息
    ShowUser getUserInfo(Integer userid);





    //管理员登陆
    User AdminLogin(String phone);
    //根据id查询用户信息
    User FindById(Integer userId);
    //后台更新信息
    Integer updateUserInfo(Map<String,Object> map);
    //新增用户
    Integer addUserInfo(AddUserVO addUserVO);
    CorporateUser getCorporateUserInfo(Integer id);

}
