package com.notebook.service.UserService;

import com.notebook.dao.*;
import com.notebook.entity.pojo.User;
import com.notebook.entity.vo.user.*;
import com.notebook.service.NewsCommService.INewsCommService;
import com.notebook.utils.EncryptUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements IUserService {

        //引用DAO或service时使用该（@Autowired）注解
        @Autowired
        private IUserDao userMapper;
        @Autowired
        private INoteDao noteMapper;
        @Autowired
        private IFavoriteDao favoriteMapper;
        @Autowired
        private IBillDao billDaoMapper;
        @Autowired
        private INewsCommDao newsCommDaoMapper;

        @Override
        public User selectByPrimaryKey(Integer id) {
            return userMapper.selectByPrimaryKey(id);
        }

        @Override
        public List<User> userList() {
            return userMapper.userList();
        }

        //用户登录
        @Override
        public List<LoginUserVO> UserLogin(LoginUserVO userVO) {
            userVO.setPassWord(EncryptUtil.md5Util(userVO.getPassWord()));
            return  userMapper.UserLogin(userVO);
        }
        //用户注册
        @Override
        public int UserReg(AddUserVO userVO) {
            userVO.setPassword(EncryptUtil.md5Util(userVO.getPassword()));
            return userMapper.UserReg(userVO);
        }
        //检测手机号是否存在
    @Override
    public boolean inspectPhone(String phone) {
        return userMapper.inspectPhone(phone)!=null;
    }

        @Override
        public boolean BackPassword(LoginUserVO userVO) {
            userVO.setPassWord(EncryptUtil.md5Util(userVO.getPassWord()));
            return userMapper.BackPassword(userVO)>0;
        }

        @Override
        public User FindByPhone(String phone) {
            return userMapper.FindByPhone(phone);
        }

    @Override
    public boolean UpdateFindByid(UpdateInfoVO record) {
        return userMapper.UpdateFindByid(record)>0;
    }

    @Override
    public boolean updatePassoWord(UpdatePwdVO pwdVO) {
            pwdVO.setPassword(EncryptUtil.md5Util(pwdVO.getPassword()));
            return userMapper.updatePassoWord(pwdVO)>0;
    }

    @Override
    public List<User> adminList() {
        return userMapper.adminList();
    }

    @Override
    public Integer addAdmin(AdminRegVo regVo) {
            regVo.setPassword(EncryptUtil.md5Util(regVo.getPassword()));
        return userMapper.addAdmin(regVo);
    }

    @Override
    public boolean UpdateInfo(AdminRegVo regVo) {
        return userMapper.UpdateInfo(regVo)>0;
    }

    @Override
    public Integer updateUserPic(String pic, Integer userId) {
        return userMapper.updateUserPic(pic,userId);
    }

    @Override
    public ShowUser getUserInfo(Integer userid) {
        return userMapper.getUserInfo(userid);
    }

    //按用户id删除用户
        @Override
        public boolean deleteByPrimaryKey(Integer userId){
            //先删除便签
            billDaoMapper.deleteByPrimaryKey(userId);
            //删除收藏
            favoriteMapper.deleteUserByPrimaryKey(userId);
            //删除账单
            noteMapper.deleteUserByPrimaryKey(userId);
            //删除评论
            newsCommDaoMapper.deleteByUserId(userId);
            return userMapper.deleteByPrimaryKey(userId)>0;
        }




    @Override
    public User AdminLogin(String phone) {
        return userMapper.AdminLogin(phone);
    }

    @Override
    public User FindById(Integer userId) {
        return userMapper.FindById(userId);
    }

    @Override
    public Integer updateUserInfo(Map<String, Object> map) {
        return userMapper.updateUserInfo(map);
    }

    @Override
    public Integer addUserInfo(AddUserVO addUserVO) {
        return userMapper.addUserInfo(addUserVO);
    }

    @Override
    public CorporateUser getCorporateUserInfo(Integer id) {
        return userMapper.getCorporateUserInfo(id);
    }


}
