package com.notebook.service.VersionService;

import com.notebook.dao.IVersionDao;
import com.notebook.entity.pojo.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/12
 */
@Service
public class VersionServiceImpl implements IVersionService {

    @Autowired
    private IVersionDao versionDaoMapper;
    @Override
    public Version checkVersion(String versionNum) {
        return versionDaoMapper.checkVersion(versionNum);
    }

    @Override
    public List<Version> queryAll() {
        return versionDaoMapper.queryAll();
    }

    @Override
    public Version findById(Integer versionId) {
        return versionDaoMapper.findById(versionId);
    }

    @Override
    public String findByVersionNum() {
        return versionDaoMapper.findByVersionNum();
    }

    @Override
    public Integer checkVersionNum(String versionNum) {
        return versionDaoMapper.checkVersionNum(versionNum);
    }

    @Override
    public Integer addVersion(Version version) {
        return versionDaoMapper.addVersion(version);
    }
}
