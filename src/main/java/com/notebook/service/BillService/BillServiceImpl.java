package com.notebook.service.BillService;

import com.notebook.dao.IBillDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/31
 */
@Service
public class BillServiceImpl implements IBillService {

    @Autowired
    private IBillDao billDaoMapper;
    @Override
    public int deleteByPrimaryKey(Integer userId) {
        return billDaoMapper.deleteByPrimaryKey(userId);
    }
}
