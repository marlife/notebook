package com.notebook.service.NewsService;

import com.notebook.dao.IFavoriteDao;
import com.notebook.dao.INewsCommDao;
import com.notebook.dao.INewsDao;
import com.notebook.dao.INewsLikeDao;
import com.notebook.entity.pojo.Favorite;
import com.notebook.entity.pojo.News;
import com.notebook.entity.pojo.User;
import com.notebook.entity.vo.news.*;
import com.notebook.entity.vo.user.ShowUser;
import com.notebook.service.UserService.IUserService;
import com.notebook.utils.DateUtil;
import com.notebook.utils.GetNewsSources;
import com.notebook.utils.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class NewsServiceImpl implements INewsService {

    //引用DAO或service时使用该（@Autowired）注解
    @Autowired
    private INewsDao newsMapper;
    @Autowired
    private INewsCommDao newsCommMapper;
    @Autowired
    private IFavoriteDao favoriteMapper;
    @Autowired
    private INewsLikeDao newsLikeMapper;

    @Autowired
    private IUserService userService;
    //查所有，返集合
    @Override
    public List<News> queryAll() {

        List<News> list = newsMapper.queryAll();
        for (News item : list) {
            //处理日期
            long time = Long.parseLong(item.getCreatetime());
            item.setCreatetime(DateUtil.LongToDate(time));
            ShowUser showUser = userService.getUserInfo(item.getNewsUserId());
            item.setShowUser(showUser);
        }
        return list;
    }

    @Override
    public boolean deleteByPrimaryKey(Integer id) {
        newsCommMapper.deleteByPrimaryKey(id);
        favoriteMapper.deleteByPrimaryKey(id);
        newsLikeMapper.deleteNewsLike(id);
        return newsMapper.deleteByPrimaryKey(id) > 0;
    }


    @Override
    public List<GetNewsVO> getNews(String type, int num) {

        try {
            return GetNewsSources.GetNews(type, num);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public boolean addNews(NewsVo addNewsVo) {
        return newsMapper.addNews(addNewsVo) > 0;
    }

    //按id查
    @Override
    public List<NewsVo> findById(Integer id) {
        return newsMapper.findById(id);
    }

    @Override
    public Integer updateByPrimaryKey(NewsVo record) {
        return newsMapper.updateByPrimaryKey(record);
    }

    @Override
    public Integer getNewsClickNumSum() {
        return newsMapper.getNewsClickNumSum();
    }

    @Override
    public News selectByPrimaryKey(Integer nwesId) {
        return newsMapper.selectByPrimaryKey(nwesId);
    }

    @Override
    public List<HomePageNewsVO> HomeNewsList() {

        List<HomePageNewsVO> homeList = newsMapper.HomeNewsList();
        for (HomePageNewsVO item : homeList) {

            //处理日期
            long time = Long.parseLong(item.getCreatetime());
            item.setCreatetime(DateUtil.LongToDate(time));
            List<String> list = StringUtils.getImgSrcList(item.getNewsPic());
            if(list.size()==0){
                item.setNewsPic("-1");
            }else{
                item.setNewsPic(list.get(0));
            }
            Integer commCount = newsCommMapper.selectCommCount(item.getNewsId());
            item.setCommCount(commCount);
            ShowUser showUser = userService.getUserInfo(item.getNewsUserId());
            item.setShowUser(showUser);
        }

        return homeList;
    }


    //获取前7天的每天新闻量
    @Override
    public List<Integer> GetNewsDayCount() {
        int[] day = {-1, -2, -3, -4, -5, -6, -7};
        List<Integer> list = new ArrayList<Integer>(7);
        for (int item : day) {
            String time = DateUtil.GetDayLong(item);
            list.add(newsMapper.GetNewsDayCount(time));
        }
        return list;
    }


    @Override
    public List<NewsRankingsVO> GetCommRankings() {
        return newsMapper.GetCommRankings();
    }

    @Override
    public List<NewsByTypeVO> FindByNewsType(Map<String, Object> newsMap) {
        List<NewsByTypeVO> list = newsMapper.FindByNewsType(newsMap);
        for (NewsByTypeVO item : list) {
            List<String> urllist = StringUtils.getImgSrcList(item.getNwesContent());
            if(urllist.size()!=0){
                item.setNewsImageUrl(urllist.get(0));
            }else{
                item.setNewsImageUrl("-1");
            }
            ShowUser showUser = userService.getUserInfo(item.getNewsUserId());
            item.setShowUser(showUser);
            item.setNewsRankNum(newsCommMapper.selectCommCount(item.getNewsId()));
        }
        return list;
    }

    @Override
    public boolean addNewsClickNum(Integer newsId) {
        return newsMapper.addNewsClickNum(newsId) > 0;
    }

    @Override
    public Integer getNewsClickNum(Integer newsId) {
        return newsMapper.getNewsClickNum(newsId);
    }

    @Override
    public Integer getNewsCount() {
        return newsMapper.getNewsCount();
    }

    @Override
    public FavoriteNewsVO findByNewsId(Integer newsId) {
        FavoriteNewsVO favorite = newsMapper.findByNewsId(newsId);
        //处理日期
        long time = Long.parseLong(favorite.getCreatetime());
        favorite.setCreatetime(DateUtil.LongToDate(time));
        //处理图片地址
        List<String> urlString= StringUtils.getImgSrcList(favorite.getNewsPic());
        if(urlString.size()!=0){
            favorite.setNewsPic(urlString.get(0));
        }else{
            favorite.setNewsPic("-1");
        }
        ShowUser showUser = userService.getUserInfo(favorite.getNewsUserId());
        favorite.setShowUser(showUser);
        //获取当前新闻的点赞数
        Integer newsLikeNum = newsLikeMapper.GetNewsLike(newsId);
        //设置当前新闻的点赞数
        favorite.setNewsLikeNum(newsLikeNum);
        //获取当前新闻的评论数
        Integer newsCommNum = newsCommMapper.selectCommCount(newsId);
        //设置当前新闻的评论数
        favorite.setCommCount(newsCommNum);
        return favorite;

    }

    @Override
    public List<LuceneNewsVo> LuceneNewsList() {
        return newsMapper.LuceneNewsList();
    }

    @Override
    public List<CarouselNewsVo> carouselList() {
        List<CarouselNewsVo> list = newsMapper.carouselList();
        for (CarouselNewsVo newsVo:list) {
           List<String> urlList =  StringUtils.getImgSrcList(newsVo.getNewsPic());
           if(urlList.size()!=0){
               newsVo.setNewsPic(urlList.get(0));
           }
        }
        return list;
    }

    @Override
    public Integer AddUserNews(UserNews userNews) {
        return newsMapper.AddUserNews(userNews);
    }

    @Override
    public List<UserNews> getUserNews(Integer userId) {
        List<UserNews> userNews = newsMapper.getUserNews(userId);
        for (UserNews item : userNews) {
            //处理图片地址
            List<String> newMap = StringUtils.getImgSrcList(item.getNewsContent());
            for (String value : newMap) {
                item.setNewsPic(value);
                break;
            }
            Integer commCount = newsCommMapper.selectCommCount(item.getNewsId());
            item.setCommNum(commCount);
            ShowUser showUser = newsMapper.getNewsUserInfo(item.getNewsUserId());
            item.setShowUser(showUser);
        }
        for (UserNews item : userNews) {
            item.setNewsContent("");
        }

        return userNews;
    }

    @Override
    public ShowUser getNewsUserInfo(Integer userId) {
        return newsMapper.getNewsUserInfo(userId);
    }


    @Override
    public List<News> getNoAdopt() {
        return newsMapper.getNoAdopt();
    }

    @Override
    public Integer shenHeNews(Map<String,Object> map) {
        return newsMapper.shenHeNews(map);
    }









    @Override
    public List<News> FindByIdGiveNews(Integer userId) {
        List<News> list = newsMapper.FindByIdGiveNews(userId);
        for (News item:list) {
            List<String> imageList = StringUtils.getImgSrcList(item.getNwesContent());
            if(imageList.size()!=0){
                String url = "http://140.143.57.236";
                item.setNewsImageUrl(url+imageList.get(0).substring(3));
            }
        }
        return list;
    }

    @Override
    public Integer getUserNewsCount(Integer userId) {
        return newsMapper.getUserNewsCount(userId);
    }

    @Override
    public Integer backUpdateNews(NewsVo newsVo) {
        return newsMapper.backUpdateNews(newsVo);
    }

    @Override
    public List<ShowUser> getTodayNewsUserInfo() {
        return newsMapper.getTodayNewsUserInfo();
    }

    @Override
    public List<ShowUser> getQuarterNewsUserInfo() {
        return newsMapper.getQuarterNewsUserInfo();
    }

    @Override
    public List<ShowUser> getWeekNewsUserInfo() {
        return newsMapper.getWeekNewsUserInfo();
    }

    @Override
    public List<ShowUser> getYearNewsUserInfo() {
        return newsMapper.getYearNewsUserInfo();
    }

    @Override
    public List<ShowUser> getMonthNewsUserInfo() {
        return newsMapper.getMonthNewsUserInfo();
    }

    @Override
    public List<CarouselNewsVo> getLastNewsInfo() {
        List<CarouselNewsVo> list = newsMapper.getLastNewsInfo();
        for (CarouselNewsVo item:list) {
            List<String> stringList = StringUtils.getImgSrcList(item.getNewsPic());
            if(stringList.size()!=0){
                String path = "http://140.143.57.236";
                item.setNewsPic(path+=stringList.get(0).substring(3));
            }else{
                item.setNewsPic("");
            }
        }
        return list;
    }

}
