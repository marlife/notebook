package com.notebook.service.NewsService;

import com.notebook.entity.pojo.News;
import com.notebook.entity.pojo.User;
import com.notebook.entity.vo.news.*;
import com.notebook.entity.vo.user.ShowUser;
import com.notebook.utils.GetNewsSources;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface INewsService {
    //查询所有反集合
    List<News> queryAll();
    //按id删除新闻
    boolean deleteByPrimaryKey(Integer newsId);

    //从接口获取新闻
    List<GetNewsVO> getNews(String type,int num);

    //添加新闻
    boolean addNews(NewsVo NewsVo);
    //按id查
    List<NewsVo> findById(Integer id);

    //按主键查询单条
    News selectByPrimaryKey(Integer nwesId);
    //获取首页新闻
    List<HomePageNewsVO> HomeNewsList();
    //获取某一天的新闻发布量
   List<Integer> GetNewsDayCount();

    //获取新闻所有评论
    List<NewsRankingsVO> GetCommRankings();
    //根据新闻类别获取新闻
    List<NewsByTypeVO> FindByNewsType(Map<String,Object> newsMap);
    //增加新闻阅读数
    boolean addNewsClickNum(Integer newsId);
    //获取新闻阅读数
    Integer getNewsClickNum(Integer newsId);

    //根据收藏里的newsid查询新闻
    FavoriteNewsVO findByNewsId(Integer newsId);
    //Lucene搜索
    List<LuceneNewsVo> LuceneNewsList();
    //客户端首页轮播图
    List<CarouselNewsVo> carouselList();
    //前台用户上传文章
    Integer AddUserNews(UserNews userNews);
    //获取用户发表的文章
    List<UserNews> getUserNews(Integer userId);
    //根据当前的新闻编号查询当前用户信息
    ShowUser getNewsUserInfo(Integer userId);


    //审核文章
    Integer shenHeNews(Map<String,Object> map);
    //获取共累计新闻发布数
    Integer getNewsCount();
    //按主键修改
    Integer updateByPrimaryKey(NewsVo record);
    //获取新闻阅读总和
    Integer getNewsClickNumSum();






    //获取用户未审核的文章
    List<News> getNoAdopt();
    //根据用户id查询发布的文章
    List<News> FindByIdGiveNews(Integer userId);
    //根据用户查询该用户发布的文章数量
    Integer getUserNewsCount(Integer userId);
    //后台修改新闻
    Integer backUpdateNews(NewsVo newsVo);

    //获取本日发前15个布文章最高的用户信息
    List<ShowUser> getTodayNewsUserInfo();
    //获取这个季度发前15个布文章最高的用户信息
    List<ShowUser> getQuarterNewsUserInfo();
    //获取本周发前15个布文章最高的用户信息
    List<ShowUser> getWeekNewsUserInfo();
    //获取本年前15个布文章最高的用户信息
    List<ShowUser> getYearNewsUserInfo();
    //获取本月前15个布文章最高的用户信息
    List<ShowUser> getMonthNewsUserInfo();
    //获取文章最少的
    List<CarouselNewsVo> getLastNewsInfo();

}
