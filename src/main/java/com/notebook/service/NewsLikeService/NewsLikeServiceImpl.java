package com.notebook.service.NewsLikeService;

import com.notebook.dao.INewsLikeDao;
import com.notebook.entity.pojo.NewsLike;
import com.notebook.entity.vo.news.NewsPointVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/5
 */
@Service
public class NewsLikeServiceImpl implements INewsLikeService {

    @Autowired
    private INewsLikeDao newsLikeDaoMapper;

    //点赞
    //先获取有没有点赞
    @Override
    public Integer GetYesNoLike(NewsLike like) {
        return newsLikeDaoMapper.GetYesNoLike(like);
    }

    @Override
    public boolean deleteClickInfo(Integer id) {
        return newsLikeDaoMapper.deleteClickInfo(id)>0;
    }

    @Override
    public boolean AddClichInfo(NewsLike like) {
        return newsLikeDaoMapper.AddClickInfo(like)>0;
    }

    @Override
    public Integer GetNewsLike(Integer newsId) {
        return newsLikeDaoMapper.GetNewsLike(newsId);
    }

    @Override
    public Integer getNewsLikeCount() {
        return newsLikeDaoMapper.getNewsLikeCount();
    }
    @Override
    public Integer deleteNewsLike(Integer newsId) {
        return newsLikeDaoMapper.deleteNewsLike(newsId);
    }

}
