package com.notebook.service.NewsLikeService;

import com.notebook.entity.pojo.NewsLike;

import java.util.Map;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/5
 */
public interface INewsLikeService {

    //点赞
    //先获取有没有点赞
    Integer GetYesNoLike(NewsLike like);
    //删除点赞信息
    boolean deleteClickInfo(Integer id);
    //添加点赞信息
    boolean AddClichInfo(NewsLike like);
    //根据id获取点赞数
    Integer GetNewsLike(Integer newsId);
    //获取共累计点赞数
    Integer getNewsLikeCount();
    //根据新闻id删除点赞
    Integer deleteNewsLike(Integer newsId);

}
