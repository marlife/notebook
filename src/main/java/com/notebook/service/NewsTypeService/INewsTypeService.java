package com.notebook.service.NewsTypeService;

import com.notebook.entity.pojo.NewsType;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/15
 */
public interface INewsTypeService {
    //查询全部新闻分类
    List<NewsType> queryAll();
    //添加新闻
    boolean insert(String newsTypeName);
    //根据id删除新闻
    boolean deleteByPrimaryKey(Integer newsTypeId);

}