package com.notebook.service.NewsTypeService;

import com.notebook.dao.INewsTypeDao;
import com.notebook.entity.pojo.NewsType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/15
 */
@Service
public class NewsTypeServiceImpl implements INewsTypeService {



    @Autowired
    private INewsTypeDao NewsTypeMapper;


    //查询全部新闻分类
    @Override
    public List<NewsType> queryAll() {
        return NewsTypeMapper.queryAll();
    }
    //添加新闻分类
    @Override
    public boolean insert(String newsTypeName) {
        return NewsTypeMapper.insert(newsTypeName)>0;
    }
    //根据id删除
    @Override
    public boolean deleteByPrimaryKey(Integer newsTypeId) {
        return NewsTypeMapper.deleteByPrimaryKey(newsTypeId)>0;
    }

}
