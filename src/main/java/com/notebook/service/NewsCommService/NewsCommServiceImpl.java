package com.notebook.service.NewsCommService;

import com.notebook.dao.INewsCommDao;
import com.notebook.entity.pojo.NewsComm;
import com.notebook.entity.vo.news.NewsCommListVo;
import com.notebook.entity.vo.news.NewsCommUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/17
 */
@Service
public class NewsCommServiceImpl implements INewsCommService {

    @Autowired
    private INewsCommDao newsCommMapper;
    @Override
    public int deleteByPrimaryKey(Integer newsId) {
        return newsCommMapper.deleteByPrimaryKey(newsId);
    }

    @Override
    public Integer selectCommCount(Integer newsId) {
        return newsCommMapper.selectCommCount(newsId);
    }

    @Override
    public Integer getNewsCommCount() {
        return newsCommMapper.getNewsCommCount();
    }

    @Override
    public Integer addNewsComm(NewsComm newsComm) {
        return newsCommMapper.addNewsComm(newsComm);
    }

    @Override
    public List<NewsComm> findByNewsId(Integer newsId) {
        return newsCommMapper.findByNewsId(newsId);
    }

    @Override
    public NewsCommUserVO getNewsCommUser(Integer userId) {
        return newsCommMapper.getNewsCommUser(userId);
    }

    @Override
    public List<NewsCommListVo> queyrAll() {
        List<NewsCommListVo> list = newsCommMapper.queyrAll();
        for (NewsCommListVo item:list) {
            item.setUserPic(item.getUserPic().substring(26));
        }
        return list;
    }

    @Override
    public Integer deleteByUserId(Integer userId) {
        return newsCommMapper.deleteByUserId(userId);
    }

    @Override
    public Integer deleteById(Integer commId) {
        return newsCommMapper.deleteById(commId);
    }
}
