package com.notebook.service.NewsCommService;

import com.notebook.entity.pojo.NewsComm;
import com.notebook.entity.vo.news.NewsCommListVo;
import com.notebook.entity.vo.news.NewsCommUserVO;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/17
 */
public interface INewsCommService {
    //按新闻id删除评论
    int deleteByPrimaryKey(Integer newsId);
    //获取评论数量
    Integer selectCommCount(Integer newsId);
    //获取共累计评论数
    Integer getNewsCommCount();
    //添加评论
    Integer addNewsComm(NewsComm newsComm);
    //根据新闻id查询全部评论
    List<NewsComm> findByNewsId(Integer newsId);
    //查询评论的用户
    NewsCommUserVO getNewsCommUser(Integer userId);
    //显示评论列表
    List<NewsCommListVo> queyrAll();
    //根据用户id删除评论
    Integer deleteByUserId(Integer userId);
    //根据主键id删除评论
    Integer deleteById(Integer commId);

}
