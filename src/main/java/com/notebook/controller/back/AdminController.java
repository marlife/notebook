package com.notebook.controller.back;


import com.notebook.dto.Dto;
import com.notebook.entity.pojo.News;
import com.notebook.entity.pojo.User;
import com.notebook.entity.vo.user.AddUserVO;
import com.notebook.service.NewsCommService.INewsCommService;
import com.notebook.service.NewsLikeService.INewsLikeService;
import com.notebook.service.NewsService.INewsService;
import com.notebook.service.UserService.IUserService;
import com.notebook.utils.DateUtil;
import com.notebook.utils.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private IUserService userService;
    @Autowired
    private INewsCommService newsCommService;
    @Autowired
    private INewsService newsService;
    @Autowired
    private INewsLikeService newsLikeService;
    //登录用户的session名称
    private final String USER_SESSION = "USER_SESSION";

    /**
     * 管理员登录
     * @param username 账号
     * @param password 密码
     * @param request 获取session
     * @param model   返回处理结果
     * @return       返回响应的页面
     * 头像问题  在本地是不需要截取的
     */
    @RequestMapping(value = "/login.md", method = RequestMethod.POST)
    public String AdminLogin(String username, String password,
                             HttpServletRequest request,
                             Model model
    ) {

        HttpSession session = request.getSession();
        User user = userService.AdminLogin(username);
        if(user==null){
            model.addAttribute("numError","账号不存在");
            model.addAttribute("phone",username);
            return "home/login";
        }
        if(user.getUserPassword().equals(EncryptUtil.md5Util(password))){
//            if(user.getUserHeadPic()!=null){
//                user.setUserHeadPic(user.getUserHeadPic().substring(26));
//            }
            session.setAttribute(USER_SESSION,user);
            return "redirect:/index.html";
        }else{
            model.addAttribute("pwdError","密码错误");
            model.addAttribute("phone",username);
            return "home/login";
        }
    }

    @RequestMapping(value = "/user_{id}.md")
    public String UserInfo(@PathVariable("id") Integer userId,Model model){
        User user = userService.FindById(userId);
        List<News> newsList =newsService.FindByIdGiveNews(userId);
        for (News news:newsList) {
            news.setNewsLikeNum(newsLikeService.GetNewsLike(news.getNewsId()));
            news.setNewsCommCount(newsCommService.selectCommCount(news.getNewsId()));
        }
        Integer count = newsService.getUserNewsCount(userId);

        model.addAttribute("newsCount",count);
        model.addAttribute("userInfo",user);
        model.addAttribute("newsList",newsList);
        return "user/profile";
    }




    //查询全部用户
    @RequestMapping(value = "/userList")
    public String userList(Model model) {
        List<User> list = userService.userList();
        model.addAttribute("userList", list);
        return "user/user_list";
    }
    //按id删除用户
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Dto deleteUser(@PathVariable("id") int id) {
        Dto dto = new Dto();
        boolean yesNo = userService.deleteByPrimaryKey(id);
        if (yesNo) {
            dto.setSuccess(true);
        }
        return dto;
    }

    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    public String updateUserInfo(
       @RequestParam(value = "password",required = false)String password,
       Integer accountStatus,
       Integer userPower,
       Integer userId
    ){
    Map<String,Object> map = new HashMap<String,Object>(0);
        map.put("password",password);
        map.put("status",accountStatus);
        map.put("power",userPower);
        map.put("userId",userId);
        Integer count = userService.updateUserInfo(map);
        return "redirect:/admin/user_"+userId+".md";
    }
    //管理员列表
    @RequestMapping(value = "/adminList.md")
    public String AdminList(Model model){
        List<User> list= userService.adminList();
        model.addAttribute("adminList",list);
        return "user/admin_list";
    }
    //新增用户信息
    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public String AddUserInfo(AddUserVO addUserVO){
        addUserVO.setRegTime(DateUtil.DateToLong());
        addUserVO.setPassword(EncryptUtil.md5Util(addUserVO.getPassword()));
        addUserVO.setUserPic("http://140.143.57.236/asset/UserHeadImage/admin.png");
        System.out.println(addUserVO);
        Integer count = userService.addUserInfo(addUserVO);
           if(addUserVO.getUserPower()==false){
               return "redirect:/admin/userList";
           }else{
               return "redirect:/admin/adminList.md";
           }
    }


}
