package com.notebook.controller.back;

import com.notebook.service.NewsLikeService.INewsLikeService;
import com.notebook.service.NewsService.INewsService;
import com.notebook.service.favoriteService.IFavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private INewsService newsService;
    @Autowired
    private INewsLikeService newsLikeService;
    @Autowired
    private IFavoriteService favoriteService;
    @RequestMapping(value = "/index.html")
    public String index(Model model){
        //新闻阅读数
        Integer newsClickNumSum = newsService.getNewsClickNumSum();
        model.addAttribute("newsClickNumSum",newsClickNumSum);
        //新闻点赞数
        Integer newsLikeCount = newsLikeService.getNewsLikeCount();
        model.addAttribute("newsLikeCount",newsLikeCount);
        //收藏数
        Integer favoriteCount = favoriteService.getFavoriteCount();
        model.addAttribute("favoriteCount",favoriteCount);
        //新闻总数
        Integer newsCount = newsService.getNewsCount();
        model.addAttribute("newsCount",newsCount);
        //获取获取七天的新闻发布量
        List<Integer> counts = newsService.GetNewsDayCount();
        model.addAttribute("counts",counts);
        return "home/index";
    }


}
