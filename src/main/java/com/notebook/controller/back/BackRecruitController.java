package com.notebook.controller.back;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.Corporate;
import com.notebook.entity.pojo.Recruit;
import com.notebook.entity.vo.recruit.BackRecruitVo;
import com.notebook.entity.vo.user.CorporateUser;
import com.notebook.service.CorporateService.ICorporateService;
import com.notebook.service.RecruitService.IRecruitService;
import com.notebook.service.UserService.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/backrecruit")
public class BackRecruitController {

    @Autowired
    private IRecruitService recruitService;
    @Autowired
    private ICorporateService corporateService;
    @Autowired
    private IUserService userService;
    @RequestMapping("/recruits.html")
    public String getBackRecruitInfo(Model model) {
        List<BackRecruitVo> lists = recruitService.getBackRecruitInfo();
        model.addAttribute("Recruits",lists);
        return "recruit/recruit_list";
    }

    @RequestMapping("/{id}.html")
    public String selectByPrimaryKey(
            @PathVariable("id") Integer id,Model model){
        Recruit recruit = recruitService.selectByPrimaryKey(id);
        Corporate corporate = corporateService.selectByPrimaryKey(recruit.getCorporateId());
        CorporateUser user = userService.getCorporateUserInfo(corporate.getUserId());
        model.addAttribute("recruit",recruit);
        model.addAttribute("corporate",corporate);
        model.addAttribute("user",user);
        return "recruit/recruit_info";
    }

}
