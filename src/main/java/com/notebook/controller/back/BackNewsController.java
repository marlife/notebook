package com.notebook.controller.back;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.Message;
import com.notebook.entity.pojo.News;
import com.notebook.entity.pojo.NewsComm;
import com.notebook.entity.pojo.NewsType;
import com.notebook.entity.vo.news.LuceneNewsVo;
import com.notebook.entity.vo.news.NewsCommUserVO;
import com.notebook.entity.vo.news.NewsVo;
import com.notebook.service.MessageService.IMessageService;
import com.notebook.service.NewsCommService.INewsCommService;
import com.notebook.service.NewsService.INewsService;
import com.notebook.service.NewsTypeService.INewsTypeService;
import com.notebook.utils.DateUtil;
import com.notebook.utils.FileUtil;
import com.notebook.utils.LuceneUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/news")
public class BackNewsController {
    @Autowired
    private INewsService newsService;
    @Autowired
    private INewsTypeService newsTypeService;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private INewsCommService newsCommService;
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Dto delete(@PathVariable("id") Integer id) {
        boolean yesno = newsService.deleteByPrimaryKey(id);
        Dto<News> dto = new Dto<News>();
        if (yesno) {
            dto.setSuccess(true);
        }
        return dto;
    }
    @RequestMapping(value = "/{id}.md")
    public String FindByIdNewsInfo(@PathVariable("id") Integer newsId,Model model){
        News news =  newsService.selectByPrimaryKey(newsId);
        List<NewsComm> list = newsCommService.findByNewsId(newsId);
        for (NewsComm item: list) {
            //查询改评论的用户
            NewsCommUserVO userVO = newsCommService.getNewsCommUser(item.getCommUserId());
            item.setNewsCommUserVO(userVO);
        }
        model.addAttribute("newsContent",news.getNwesContent());
        model.addAttribute("newsComms",list);
        return "news/news_info";
    }
    //对新闻的操作
    @RequestMapping(value = "/newsList.md")
    public String JumpNews(Model model) {
        List<News> list = newsService.queryAll();
        model.addAttribute("newslist", list);
        return "news/news_list";
    }

    @RequestMapping(value = "/editor",method = RequestMethod.POST)
    public String add(NewsVo newsVo) {

        System.out.println(newsVo);
        if(newsVo.getNewsId()!=null){
            Integer num = newsService.backUpdateNews(newsVo);
            String path = "/usr/tomcat/webapps/notebook///../asset/LuceneData";
            if(FileUtil.deleteDirectory(path)){
                List<LuceneNewsVo> list = newsService.LuceneNewsList();
                LuceneUtil.IndexWrite(path,list);

            }
        }else{
            newsVo.setCreateTime(DateUtil.DateToLong());
            boolean yesno = newsService.addNews(newsVo);
            String path = "/usr/tomcat/webapps/notebook///../asset/LuceneData";
            if(FileUtil.deleteDirectory(path)){
                List<LuceneNewsVo> list = newsService.LuceneNewsList();
                LuceneUtil.IndexWrite(path,list);

            }
        }

        return "redirect:/admin/news/newsList.md";
    }

    @RequestMapping(value = "/auditlist.md")
    public String JumpNewsAudit(Model model){
        List<News> list = newsService.getNoAdopt();
        model.addAttribute("userNews",list);
        return "news/news_audit";
    }
    @RequestMapping(value = "/audit.md",method = RequestMethod.POST)
    public String ShenHeNews(Message message){
        Map<String,Object> map = new HashMap<String,Object>(0);
        map.put("isrelease",message.getIsrelease());
        map.put("newsId",message.getNid());
        map.put("userId",message.getUid());
        Integer count = newsService.shenHeNews(map);
        if(count!=null){
            message.setMessageUserId(message.getUid());
            message.setMessageTime(DateUtil.DateToLong());
            Integer mess = messageService.addMessage(message);

        }
       return "redirect:/admin/news/auditlist.md";
    }
    @RequestMapping(value = "/news_add.html")
    public String JumpNewsAdd(Model model){
        List<NewsType> list = newsTypeService.queryAll();
        model.addAttribute("types",list);
        return "news/news_add";
    }
    @RequestMapping(value = "/update/{id}.html")
    public String updateNews(@PathVariable("id")Integer id,Model model){
        List<NewsVo> newsVos = newsService.findById(id);
        newsVos.get(0).setNewsId(id);
        List<NewsType> list = newsTypeService.queryAll();
        model.addAttribute("types",list);
        model.addAttribute("news", newsVos.get(0));
        return "news/news_add";
    }


}
