package com.notebook.controller.back;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.Proposal;
import com.notebook.service.ProposalService.IProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/opinion")
public class OpinionController {

    @Autowired
    private IProposalService proposalService;

    @RequestMapping(value = "/list.html")
    public String OpinionList(Model model){
        List<Proposal> list = proposalService.queryAll();
        model.addAttribute("opinionList",list);
        return "chat/opinion";
    }
    @RequestMapping(value = "/setRead",method = RequestMethod.POST)
    @ResponseBody
    public Dto SettingIsRead(Integer id){
        Dto dto = new Dto();
        Integer num = proposalService.settingRead(id);
        if(num!=null){
            dto.setSuccess(true);
        }
        return dto;
    }
    //获取未读的留言数量
    @RequestMapping(value = "/getCount",method = RequestMethod.POST)
    @ResponseBody
    public Dto GetUnreadInfoCount(){
        Dto dto = new Dto();
        Integer count = proposalService.GetUnreadInfoCount();
        if(count>0){
            dto.setData(count);
            dto.setSuccess(true);
        }

        return dto;
    }

    //获取未读的留言内容
    @RequestMapping(value = "/noReads",method = RequestMethod.POST)
    @ResponseBody
    public Dto GetNoReadList(){
        Dto dto = new Dto();
        List<Proposal> lists = proposalService.getNoReadList();
        dto.setData(lists);
        return dto;
    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public Dto delOpinion(Integer id){
        Dto dto = new Dto();
        Integer num = proposalService.delById(id);
        if(num!=null){
            dto.setSuccess(true);
        }
        return dto;
    }
}
