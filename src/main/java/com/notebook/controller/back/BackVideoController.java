package com.notebook.controller.back;

import com.notebook.dao.IVideoMapper;
import com.notebook.dto.Dto;
import com.notebook.entity.pojo.NewsType;
import com.notebook.entity.pojo.Video;
import com.notebook.service.ChapterInfoService.IChapterInfoService;
import com.notebook.service.NewsTypeService.INewsTypeService;
import com.notebook.service.VideoService.IVideoService;
import com.notebook.utils.DateUtil;
import com.notebook.utils.UpLoadUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/video")
public class BackVideoController {

    @Autowired
    private INewsTypeService newsTypeService;
    @Autowired
    private IVideoService videoService;

    @Autowired
    private IChapterInfoService chapterInfoService;

    @RequestMapping(value = "/add.html")
    public String JumpVideoAdd(Model model) {
        List<NewsType> list = newsTypeService.queryAll();
        model.addAttribute("types", list);
        return "video/video_edit";
    }

    @RequestMapping("/list.html")
    public String JumoVideoList(Model model) {
        List<Video> videos = videoService.queryAll();
        model.addAttribute("videoList",videos);
        return "video/video_list";
    }

    @RequestMapping(value = "/add.md", method = RequestMethod.POST)
    public String insertSelective(Video video,
                                  @RequestParam(name = "cover", required = false) MultipartFile file,
                                  HttpServletRequest request,
                                  Model model
    ) {
        video.setVideoTime(DateUtil.DateToLong());
        video.setVideoState(1);


        if (file.isEmpty()) {
            model.addAttribute("info", "您没有上传视频封面!");
            return "video/video_edit";
        }
        String appRoot = request.getSession().getServletContext().getRealPath("") + File.separator;//定义上传路径
        //获取文件后缀
        String suffix = FilenameUtils.getExtension(file.getOriginalFilename());
        if (suffix.contentEquals("jpg")
                || suffix.equalsIgnoreCase("png")
                || suffix.equalsIgnoreCase("jpeg")
                || suffix.equalsIgnoreCase("pneg")
                ) {
            String fileName = "/../asset/VideoCover/" + DateUtil.getDateDirName()+"/"+DateUtil.getDateFileName() + "." + suffix;
            String path = appRoot + fileName;
            File file1 = new File(path);
            if (!file1.exists()) {
                file1.mkdirs();
            }
            try {
                file.transferTo(file1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String videoCoverPath = "http://140.143.57.236" + fileName.substring(3);
            video.setVideoCover(videoCoverPath);
            Integer count = videoService.insertSelective(video);
            if (count != null) {

                return "redirect:/video/list.html";
            }
            model.addAttribute("info", "上传失败，发现未知的错误！");
            return "video/video_edit";
        } else {
            model.addAttribute("info", "文件格式不正确，请重新上传！");
            return "video/video_edit";
        }

    }

    @RequestMapping(value = "/upload")
    @ResponseBody
    public Map<String, Object> upload(HttpServletRequest request,
                                      HttpServletResponse response) {
        String pathName = "/../asset/VideoImage/" +DateUtil.getDateDirName()+"/"+ DateUtil.getDateFileName() + "/";

        return UpLoadUtil.upLoad(pathName, request, response);
    }

    @RequestMapping(value = "/del/{id}.html")
    public String deleteById(@PathVariable("id") Integer id){
        Integer count = videoService.deleteByPrimaryKey(id);
        Integer count1 = chapterInfoService.deleteByPrimaryKey(id);
        return "redirect:/video/list.html";
    }
}
