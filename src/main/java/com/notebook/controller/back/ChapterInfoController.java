package com.notebook.controller.back;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.ChapterInfo;
import com.notebook.service.ChapterInfoService.IChapterInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/chapter")
public class ChapterInfoController {

    @Autowired
    private IChapterInfoService chapterInfoService;


    @RequestMapping(value = "/info/{id}.md",method = RequestMethod.POST)
    @ResponseBody
    public Dto findByQueryAll(@PathVariable("id") Integer id){
        Dto dto = new Dto();
        List<ChapterInfo> lists = chapterInfoService.findQueryAll(id);
        if(lists.size()!=0){
            dto.setSuccess(true);
            dto.setData(lists);
        }
        return dto;
    }

    @RequestMapping(value = "/add.html",method = RequestMethod.POST)
    public String AddChapterInfo(ChapterInfo chapterInfo){
        Integer count = chapterInfoService.insert(chapterInfo);
        return "redirect:/video/list.html";
    }
    @RequestMapping(value = "/del/{id}.html")
    public String  deleteById(@PathVariable("id")Integer id){
        Integer count = chapterInfoService.deleteById(id);
        return "redirect:/video/list.html";

    }
}
