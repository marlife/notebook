package com.notebook.controller.app;

import com.notebook.entity.pojo.Corporate;
import com.notebook.entity.vo.user.CorporateUser;
import com.notebook.service.CorporateService.ICorporateService;
import com.notebook.service.UserService.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/corporate")
public class CorporateController {

    @Autowired
    private ICorporateService corporateService;

    @Autowired
    private IUserService userService;

    @RequestMapping("/{id}.html")
    public String selectByPrimaryKey(
            @PathVariable("id") Integer id,
            Model model
    ){
        Corporate corporate = corporateService.selectByPrimaryKey(id);
        CorporateUser user = userService.getCorporateUserInfo(corporate.getUserId());
        model.addAttribute("user",user);
        model.addAttribute("corporate",corporate);
        return "recruit/corporate_info";
    }

}
