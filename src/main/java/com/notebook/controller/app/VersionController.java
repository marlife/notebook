package com.notebook.controller.app;

import com.notebook.dao.INewsCommDao;
import com.notebook.dto.Dto;
import com.notebook.entity.pojo.Version;
import com.notebook.service.VersionService.VersionServiceImpl;
import com.notebook.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/version")
public class VersionController {




    @Autowired
    private VersionServiceImpl versionService;


    @RequestMapping(value = "/checkVersion", method = RequestMethod.POST)
    @ResponseBody
    public Dto checkVersion(@RequestParam("versionNum") String versionNum) {
        Dto dto = new Dto();
      Version version = versionService.checkVersion(versionNum);
        if (version != null) {
            dto.setSuccess(true);
            dto.setData(version);
        }
        return dto;
    }

    @RequestMapping
    @ResponseBody
    public Dto queryAll(){
        Dto dto = new Dto();
        List<Version> list = versionService.queryAll();
        dto.setData(list);
        return dto;
    }



    @RequestMapping(value = "/findById",method = RequestMethod.POST)
    @ResponseBody
    public Dto findById(@RequestParam("versionId")Integer versionId){
        Dto dto = new Dto();
        Version version = versionService.findById(versionId);
        if(version!=null){
            dto.setSuccess(true);
            dto.setData(version);
        }
        return dto;
    }
    @RequestMapping(value = "/jumpUpdate")
    public String jumpUpdate(Model model,Integer versionId){
       Version version = versionService.findById(versionId);
        model.addAttribute("version",version);
        return "system/version_update";
    }

    @RequestMapping(value = "/checkVersionNum",method = RequestMethod.POST)
    @ResponseBody
    public Dto checkVersionNum(String versionNum){
        Dto dto = new Dto();
        Integer count = versionService.checkVersionNum(versionNum);
        if(count!=null){
            dto.setSuccess(true);
        }

        return dto;
    }

    @RequestMapping(value = "/addVersion",method = RequestMethod.POST)
    @ResponseBody
    public Dto AddVersion(Version version){
        Dto dto  = new Dto();
        version.setVersionTime(DateUtil.DateToLong());
       Integer count =  versionService.addVersion(version);
       if(count!=null){
           dto.setSuccess(true);
       }
       return dto;
    }

}
