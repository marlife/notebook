package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.Proposal;
import com.notebook.service.ProposalService.ProposalServiceImpl;
import com.notebook.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/31
 */
@Controller
@RequestMapping("/proposal")
public class ProposalController {
    @Autowired
    private ProposalServiceImpl proposalService;



    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Dto add(@RequestParam("proposalContent") String proposalContent){
        Proposal proposal = new Proposal(proposalContent,DateUtil.DateToLong());
        Dto dto = new Dto();
        boolean yesNo = proposalService.insert(proposal);
        if(yesNo){
            dto.setSuccess(true);
        }
        return dto;
    }



}
