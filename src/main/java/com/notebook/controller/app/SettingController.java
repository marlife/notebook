package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.Version;
import com.notebook.service.VersionService.VersionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/26
 */
@Controller
@RequestMapping("/setting")
public class SettingController {

    @Autowired
    private VersionServiceImpl versionService;

    @RequestMapping(value = "system",method = RequestMethod.GET)
    public String system(Model model){
        //获取当前版本号
        model.addAttribute("versionNum",versionService.findByVersionNum());
        return "system/system-base";
    }

    @RequestMapping(value = "/queryAll",method = RequestMethod.GET)
    public String historicalVersion(Model model){
        List<Version> list = versionService.queryAll();
        model.addAttribute("verall",list);
        return "system/hist_version";
    }
}
