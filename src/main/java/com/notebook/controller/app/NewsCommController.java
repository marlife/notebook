package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.News;
import com.notebook.entity.pojo.NewsComm;
import com.notebook.entity.vo.news.NewsCommListVo;
import com.notebook.entity.vo.news.NewsCommUserVO;
import com.notebook.service.NewsCommService.NewsCommServiceImpl;
import com.notebook.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/7
 */
@Controller
@RequestMapping("/newsComm")
public class NewsCommController {

    @Autowired
    private NewsCommServiceImpl newsCommService;

    @RequestMapping(value = "/addNewsComm",method = RequestMethod.POST)
    @ResponseBody
    public Dto addNewsComm(
            @RequestParam("content")String content,
            @RequestParam("newsId")Integer newsId,
            @RequestParam("userId")Integer userId
    ){
        Dto dto = new Dto();
        //查询该新闻的所有评论
        NewsComm newsComm = new NewsComm(content, DateUtil.DateToLong(),newsId,userId);
        Integer num = newsCommService.addNewsComm(newsComm);
        if(num>0){
            dto.setSuccess(true);
            List<NewsComm> list = newsCommService.findByNewsId(newsId);
            for (NewsComm item: list) {
                //查询改评论的用户
                NewsCommUserVO userVO = newsCommService.getNewsCommUser(item.getCommUserId());
                item.setNewsCommUserVO(userVO);
            }

            dto.setData(list);
        }

        return dto;
    }

    @RequestMapping
    public String jumpNewsComm(Model model){
        List<NewsCommListVo> list = newsCommService.queyrAll();
        model.addAttribute("commList",list);
        return "news/newsComm/news_comm";
    }
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    @ResponseBody
    public Dto deleteComm(@PathVariable("id")Integer id){
        Dto dto = new Dto();
        Integer num = newsCommService.deleteById(id);
        if(num!=null){
            dto.setSuccess(true);
        }
        return dto;
    }
}
