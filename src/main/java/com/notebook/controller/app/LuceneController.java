package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.entity.vo.news.LuceneNewsVo;
import com.notebook.service.LuceneService.LuceneServiceImpl;
import com.notebook.service.NewsService.NewsServiceImpl;
import com.notebook.utils.FileUtil;
import com.notebook.utils.LuceneUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/10
 */
@Controller
@RequestMapping("/lucene")
public class LuceneController {

    @Autowired
    private LuceneServiceImpl luceneService;
    @Autowired
    private NewsServiceImpl newsService;

    @RequestMapping(value = "/search",method = RequestMethod.POST)
    @ResponseBody
    public Dto searchTile(@RequestParam("title") String title, HttpServletRequest request){
        Dto dto = new Dto();
        String fileName = "/../asset/LuceneData";
        String appRoot = request.getSession().getServletContext().getRealPath("") + File.separator;
        String path =appRoot+fileName;
        List<LuceneNewsVo> list =newsService.LuceneNewsList();
        List<LuceneNewsVo> newsVoList = LuceneUtil.search(path,title);
        if(newsVoList.size()!=0){
            dto.setData(newsVoList);
        }
        dto.setMessage(path);
        return dto;
    };

    @RequestMapping(value = "indexing",method = RequestMethod.POST)
    @ResponseBody
    public Dto DeleteIndexing(){
        Dto dto = new Dto();
        String path = "/usr/tomcat/webapps/notebook///../asset/LuceneData";
       if(FileUtil.deleteDirectory(path)){
           List<LuceneNewsVo> list = newsService.LuceneNewsList();
           if(LuceneUtil.IndexWrite(path,list)){
               dto.setSuccess(true);

           }
       }

        return dto;
    }


}
