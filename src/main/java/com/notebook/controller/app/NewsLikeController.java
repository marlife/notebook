package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.NewsLike;
import com.notebook.service.NewsLikeService.NewsLikeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/5
 */
@Controller
@RequestMapping("/newsLike")
public class NewsLikeController {

    @Autowired
    private NewsLikeServiceImpl newsLikeService;

    @RequestMapping
    @ResponseBody
    public Dto NewsClickLike(@RequestParam("newsId")Integer newsId,@RequestParam("deviceId") String deviceId){
        Dto dto = new Dto();
        NewsLike like = new NewsLike(newsId,deviceId);
        //接收点赞表返回的主键
        Integer num = newsLikeService.GetYesNoLike(like);
        //判断点赞表是否包含这条数据
        if(num==null){
            //如果没有添加这信息
            boolean addyesno =  newsLikeService.AddClichInfo(like);
        }else{
            //如果有就删除那条数据
            boolean deleteyesno =  newsLikeService.deleteClickInfo(num);
        }
        //操作成功 返回true
        dto.setSuccess(true);
        //最后带回点赞数
        dto.setData(newsLikeService.GetNewsLike(like.getLikeNewsId()));
        return dto;
    }



}
