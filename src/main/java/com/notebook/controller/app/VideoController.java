package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.Video;
import com.notebook.entity.vo.video.VideoVo;
import com.notebook.service.VideoService.IVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/appvideo")
public class VideoController {

    @Autowired
    private IVideoService videoService;


    @RequestMapping(value = "/leastWatch",method = RequestMethod.POST)
    @ResponseBody
    public Dto getTheLeastWatch(){
        Dto dto = new Dto();
        List<VideoVo> lists = videoService.getTheLeastWatch();
        dto.setData(lists);
        return dto;
    }
    @RequestMapping(value = "/mostWatch",method = RequestMethod.POST)
    @ResponseBody
    public Dto getTheMostWatch(){
        Dto dto = new Dto();
        List<VideoVo> lists = videoService.getTheMostWatch();
        dto.setData(lists);
        return dto;
    }
    @RequestMapping(value = "/latestRelease",method = RequestMethod.POST)
    @ResponseBody
    public Dto getTheLatestRelease(){
        Dto dto = new Dto();
        List<Video> lists = videoService.getTheLatestRelease();
        dto.setData(lists);
        return dto;
    }
}
