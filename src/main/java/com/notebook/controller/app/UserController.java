package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.entity.pojo.User;
import com.notebook.entity.vo.user.*;
import com.notebook.service.UserService.UserServiceImpl;
import com.notebook.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    //获取缓存对象
    private  Jedis jedis = JRedisUtil.getJedis();

    //管理员退出操作
    @RequestMapping(value = "/signout", method = RequestMethod.GET)
    public String Signout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return "home/index";
    }








    //弹出新增用户页面
    @RequestMapping(value = "/JumpUserAdd", method = RequestMethod.GET)
    public String JumpUserAdd() {
        return "user/user_add";
    }




    //用户修改密码
    @RequestMapping(value = "/upPwd", method = RequestMethod.POST)
    @ResponseBody
    public Dto changePassword(@RequestParam("phone")String phone,@RequestParam("pwd") String pwd) {
        Dto dto = new Dto();
        UpdatePwdVO pwdVO = new UpdatePwdVO(phone,pwd);
        boolean yesno = userService.updatePassoWord(pwdVO);
        if (yesno) {
            dto.setSuccess(true);
        }
        return dto;
    }


    //用户登录
    @RequestMapping(value = "/userLogin", method = RequestMethod.POST)
    @ResponseBody
    public Dto<Integer> UserLogin(@RequestParam("pwd") String pwd, @RequestParam("phone") String phone) {
        Dto dto = new Dto();
        LoginUserVO userVO = new LoginUserVO(pwd, phone);
        List<LoginUserVO> userVOS = userService.UserLogin(userVO);
        if (userVOS.size() == 0) {
            dto.setData(-1);
        } else {
            for (LoginUserVO item : userVOS) {
                if (item.getAccountStatus() == false) {
                    dto.setData(0);
                } else {
                    dto.setData(1);
                    dto.setMessage(item.getId() + "");
                }
            }
        }
        return dto;

    }

    //用户注册
    @RequestMapping(value = "/userReg", method = RequestMethod.POST)
    @ResponseBody
    public Dto add(@RequestParam("pwd") String pwd,@RequestParam("code") String code,@RequestParam("phone") String phone) {
        Dto dto = new Dto();
        AddUserVO userVO = new AddUserVO(pwd, phone, DateUtil.DateToLong());
        userVO.setUserPic("http://140.143.57.236/asset/UserHeadImage/admin.png");
        userVO.setUserName(userVO.getPhone());
        if (jedis.get(phone+"1").equals(code)) {
            int num = userService.UserReg(userVO);
            if (num > 0) {
                dto.setMessage(userVO.getId() + "");
                dto.setData(1);

            }
        }else{
            dto.setData(0);
        }
      return dto;
    }
    @RequestMapping(value = "/getCode",method = RequestMethod.POST)
    @ResponseBody
    public Dto GetCode(@RequestParam("phone")String phone){
        Dto dto = new Dto();
        if (userService.inspectPhone(phone)) {
            dto.setData(-1);
        } else {
            if (jedis.smembers(phone).size() < 9) {
                PhoneCodeVo codeVo = SendInfoUtil.Get(phone);
                if (("210").equals(codeVo.getContent())) {
                    dto.setData(210);
                } else {
                    Integer num = codeVo.getCodeNum();
                    jedis.sadd(phone, num + "");
                    jedis.setex(phone+"1",600,num + "");
                    dto.setData(1);
                    dto.setMessage("发送成功!!!");
                }
            } else {
                dto.setData(0);
            }
        }
        return dto;
    }

    //用户找回密码验证码
    @RequestMapping(value = "/backCodePwd",method = RequestMethod.POST)
    @ResponseBody
    public Dto backCodePwd(@RequestParam("phone")String phone){
        Dto dto = new Dto();
        if (userService.inspectPhone(phone)) {
            if (jedis.smembers(phone).size() < 9) {
                PhoneCodeVo codeVo = SendInfoUtil.Get(phone);
                if (("210").equals(codeVo.getContent())) {
                    dto.setData(210);
                } else {
                    Integer num = codeVo.getCodeNum();
                    jedis.sadd(phone, num + "");
                    jedis.setex(phone+"2",600,num + "");
                    dto.setData(1);
                }
            } else {
                dto.setData(0);
            }
        } else {
            dto.setData(-1);
        }
        return dto;
    }
    //用户找回密码
    @RequestMapping(value = "/backPwd", method = RequestMethod.POST)
    @ResponseBody
    public Dto backPwd(@RequestParam("code")String code,@RequestParam("pwd") String pwd, @RequestParam("phone") String phone) {
        Dto dto = new Dto();
        LoginUserVO userVO = new LoginUserVO(pwd, phone);
            if(code.equals(jedis.get(phone+"2"))&&jedis.get(phone+"2")!=null){
                if (userService.BackPassword(userVO)) {
                    dto.setData("1");
                }
            }else{
                dto.setData("38");
            }
        return dto;
    }

    //按手机号查询用户信息
    @RequestMapping(value = "/byphone", method = RequestMethod.POST)
    @ResponseBody
    public Dto FindByPhoen(@RequestParam("phone") String phone) {
        Dto dto = new Dto();
        User user = userService.FindByPhone(phone);
        if (user != null) {
            dto.setData(user);
        }
        return dto;

    }
    @RequestMapping(value = "/findByUserId",method = RequestMethod.POST)
    @ResponseBody
    public Dto FindById(@RequestParam("id") Integer id){
        Dto dto = new Dto();
        User user = userService.selectByPrimaryKey(id);
        dto.setData(user);
        return dto;
    }

    //修改个人资料
    @RequestMapping(value = "/updateInfo", method = RequestMethod.POST)
    @ResponseBody
    public Dto UpdateUserInfo(UpdateInfoVO infoVO) {
        Dto dto = new Dto();
        boolean yesno = userService.UpdateFindByid(infoVO);
        if (yesno) {
            dto.setSuccess(true); 
        }
        return dto;

    }



    @RequestMapping(value = "/adminReg",method = RequestMethod.POST)
    @ResponseBody
    public Dto AdminReg(AdminRegVo regVo){
        Dto dto = new Dto();
        regVo.setRegTime(DateUtil.DateToLong());
        Integer num =  userService.addAdmin(regVo);
        if(num!=null){
           dto.setSuccess(true);
        }
        return dto;
    }
    @RequestMapping(value = "/loginThone",method = RequestMethod.POST)
    @ResponseBody
    public Dto loginThone(@RequestParam("phone") String phone){
        Dto dto = new Dto();
        if(!userService.inspectPhone(phone)){
            dto.setSuccess(true);
        }
        return dto;
    }
    //按手机号查询信息并跳转到修改信息页面
    @RequestMapping(value = "/JumpUpdate",method = RequestMethod.GET)
    public String JumpUpdateInfo(@RequestParam("phone") String phone,Model model){
        User user = userService.FindByPhone(phone);
        if (user != null) {
            model.addAttribute("info",user);
        }
        return "system/admin-update";
    }
    //管理员修改信息
    @RequestMapping(value = "/UpdateInfo",method = RequestMethod.POST)
    @ResponseBody
    public Dto UpdateInfo(AdminRegVo regVo){
        Dto dto =new Dto();
        if(userService.UpdateInfo(regVo))dto.setSuccess(true);
        return dto;
    }


    @RequestMapping(value = "/userPic",method = RequestMethod.POST)
    @ResponseBody
    public Dto UpLoadUserPic(
            @RequestParam("userId")Integer userId,
            @RequestParam("phone")String phone,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Dto dto = new Dto();
        String path = "/../asset/UserHeadImage/"+phone+"/"+phone+NumberUtil.getRandom10Num();
        Map<String, Object> map = UpLoadUtil.UserPic(path,request,response);
        String urlpic = map.get("url").toString();
         if(urlpic!=null){
            String url1 = "http://140.143.57.236/"+  urlpic.substring(4);;
            userService.updateUserPic(url1,userId);
            dto.setData(url1);
            dto.setSuccess(true);
       }
        return dto;
    }

}
