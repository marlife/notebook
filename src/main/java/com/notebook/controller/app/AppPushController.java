package com.notebook.controller.app;

import com.notebook.dto.Dto;
import com.notebook.service.AppPushService.AppPushServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/20
 */
@Controller
@RequestMapping("/push")
public class AppPushController {

    @Autowired
    private AppPushServiceImpl appPushService;

    @RequestMapping(value = "/pushInfo",method = RequestMethod.POST)
    @ResponseBody
    public Dto PushInfo(
            @RequestParam("title") String title,
            @RequestParam("content") String content,
            @RequestParam("url") String url){
        Dto dto = new Dto();


        return dto;
    }
    @RequestMapping
    public String jumpAppPush(){
        return "system/app_push";
    }
}
