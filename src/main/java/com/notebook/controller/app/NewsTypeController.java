package com.notebook.controller.app;

import com.notebook.dto.Dto;

import com.notebook.entity.pojo.NewsType;
import com.notebook.service.NewsTypeService.NewsTypeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/type")
public class NewsTypeController {


    @Autowired
    private NewsTypeServiceImpl newsTypeService;


    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public Dto DtoqueryAll() {
        Dto dto = new Dto();
        List<NewsType> list = newsTypeService.queryAll();
        if (list != null) {
            dto.setSuccess(true);
            dto.setData(list);
        }
        return dto;
    }


    @RequestMapping(value = "/newsType",method = RequestMethod.GET)
    public String newsSet(Model model){
        List<NewsType> list = newsTypeService.queryAll();
        model.addAttribute("newsTypeList",list);
        return "news/NewsManage/news_manage";
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Dto delete(@PathVariable("id") Integer id) {
        Dto dto = new Dto();
        Boolean yesNo = newsTypeService.deleteByPrimaryKey(id);
        if (yesNo){
            dto.setSuccess(true);
        }
        return  dto;
    }
    //添加新闻分类
    @RequestMapping(value = "/addNewsType",method = RequestMethod.POST)
    @ResponseBody
    public Dto add(@RequestParam("newsTypeName")String newsTypeName) {
        Dto dto = new Dto();
        boolean yesNo = newsTypeService.insert(newsTypeName);
        if(yesNo){
            dto.setSuccess(true);
        }
        return dto;

    }

}