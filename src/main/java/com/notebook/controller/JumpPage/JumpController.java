package com.notebook.controller.JumpPage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JumpController {
    @RequestMapping(value = "/user_analysis.html")
    public String JumpUserAnalysis(){
        return "user/user_analysis";
    }
    @RequestMapping(value = "/news_analysis.html")
    public String JumpNewsAnalysis(){
        return "news/news_analysis";
    }
    @RequestMapping(value = "chatroom.html")
    public String JumpChatRoom(){
        return "chat/chatroom";
    }
    @RequestMapping(value = "/login.html")
    public String JumpLogin(){
        return "home/login";
    }
    @RequestMapping(value = "/404.html")
    public String Jump404(){
        return "error/404";
    }
    @RequestMapping(value = "/setting.html")
    public String JumpSetting(){
        return "setting/setting";
    }

}
