package com.notebook.dto;

public class Dto<T> {
    private boolean success;
    private T data;
    private String message;

    public Dto(){}
    public Dto(boolean success) {
        this.success = success;
    }
    public Dto(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
    public Dto(boolean success, T data, String message) {
        this.success = success;
        this.data = data;
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Dto{" +
                "success=" + success +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}
