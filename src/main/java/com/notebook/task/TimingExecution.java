package com.notebook.task;

import com.notebook.entity.vo.news.LuceneNewsVo;
import com.notebook.service.NewsService.NewsServiceImpl;
import com.notebook.utils.FileUtil;
import com.notebook.utils.JRedisUtil;
import com.notebook.utils.LuceneUtil;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

import java.util.Date;
import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/10
 */
@Component
public class TimingExecution {

    @Autowired
    private NewsServiceImpl newsService;


//    @Scheduled(cron ="0 0 10 * * ?")//每天早上10点定时生成索引
//    public void taskIndexWrite(){
//        String path = "/usr/tomcat/webapps/notebook///../asset/LuceneData";
//        if(FileUtil.deleteDirectory(path)){
//            List<LuceneNewsVo> list = newsService.LuceneNewsList();
//            LuceneUtil.IndexWrite(path,list);
//        }
//
//
//
//    }


    //每天晚上12点定时执行清除所有缓存
    @Scheduled(cron = "0 0 0 * * ?")
    public void taskPhoneCode() {
        Jedis jedis = JRedisUtil.getJedis();
        jedis.flushAll();


    }

}
