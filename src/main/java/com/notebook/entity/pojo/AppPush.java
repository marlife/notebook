package com.notebook.entity.pojo;
public class AppPush {
    private String title;
    private String text;
    private String pictureStyle;
    private String longText;
    private Long InfoTime;

    public AppPush() {
    }

    public Long getInfoTime() {
        return InfoTime;
    }

    public void setInfoTime(Long infoTime) {
        InfoTime = infoTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPictureStyle() {
        return pictureStyle;
    }

    public void setPictureStyle(String pictureStyle) {
        this.pictureStyle = pictureStyle;
    }

    public String getLongText() {
        return longText;
    }

    public void setLongText(String longText) {
        this.longText = longText;
    }
}
