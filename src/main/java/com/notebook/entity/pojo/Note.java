package com.notebook.entity.pojo;

public class Note {
    private Integer noteId;

    private Long noteCreatetime;

    private Integer noteUserId;

    private String noteContent;

    public Integer getNoteId() {
        return noteId;
    }

    public void setNoteId(Integer noteId) {
        this.noteId = noteId;
    }

    public Long getNoteCreatetime() {
        return noteCreatetime;
    }

    public void setNoteCreatetime(Long noteCreatetime) {
        this.noteCreatetime = noteCreatetime;
    }

    public Integer getNoteUserId() {
        return noteUserId;
    }

    public void setNoteUserId(Integer noteUserId) {
        this.noteUserId = noteUserId;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }
}