package com.notebook.entity.pojo;

public class NewsType {
    private Integer newsTypeId;

    private String newsTypeName;

    public Integer getNewsTypeId() {
        return newsTypeId;
    }

    public void setNewsTypeId(Integer newsTypeId) {
        this.newsTypeId = newsTypeId;
    }

    public String getNewsTypeName() {
        return newsTypeName;
    }

    public void setNewsTypeName(String newsTypeName) {
        this.newsTypeName = newsTypeName;
    }

    @Override
    public String toString() {
        return "NewsType{" +
                "newsTypeId=" + newsTypeId +
                ", newsTypeName='" + newsTypeName + '\'' +
                '}';
    }
}