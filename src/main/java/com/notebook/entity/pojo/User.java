package com.notebook.entity.pojo;

public class User {
    private int userId;

    private String userName;

     private String userPassword;

    private boolean userSex;

    private String userPhone;

    private String userQq;

    private String userEmali;

    private String userAddress;

    private String userHeadPic;

    private boolean userPower;

    private boolean accountStatus;

    private String userAutograph;

    private Long regTime;

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +

                ", userPassword='" + userPassword + '\'' +
                ", userSex=" + userSex +
                ", userPhone='" + userPhone + '\'' +
                ", userQq='" + userQq + '\'' +
                ", userEmali='" + userEmali + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", userHeadPic='" + userHeadPic + '\'' +
                ", userPower=" + userPower +
                ", accountStatus=" + accountStatus +
                ", userAutograph='" + userAutograph + '\'' +
                '}';
    }

    public Long getRegTime() {
        return regTime;
    }

    public void setRegTime(Long regTime) {
        this.regTime = regTime;
    }

    public String getUserAutograph() {
        return userAutograph;
    }

    public void setUserAutograph(String userAutograph) {
        this.userAutograph = userAutograph;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public boolean getUserSex() {
        return userSex;
    }

    public void setUserSex(boolean userSex) {
        this.userSex = userSex;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserQq() {
        return userQq;
    }

    public void setUserQq(String userQq) {
        this.userQq = userQq;
    }

    public String getUserEmali() {
        return userEmali;
    }

    public void setUserEmali(String userEmali) {
        this.userEmali = userEmali;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserHeadPic() {
        return userHeadPic;
    }

    public void setUserHeadPic(String userHeadPic) {
        this.userHeadPic = userHeadPic;
    }

    public boolean getUserPower() {
        return userPower;
    }

    public void setUserPower(boolean userPower) {
        this.userPower = userPower;
    }

    public boolean getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(boolean accountStatus) {
        this.accountStatus = accountStatus;
    }
}