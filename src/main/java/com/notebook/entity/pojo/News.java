package com.notebook.entity.pojo;

import com.notebook.entity.vo.user.ShowUser;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class News {
    private Integer newsId;
    private String newsTitle;
    private String createtime;
    private String newsKey;
    private int clickNumber;
    private String nwesContent;
    private NewsType type;
    private String newsImageUrl;//新闻图片地址
    private Integer isRelease; //是否审核
    private Integer newsCommCount; //评论数
    private int newsLikeNum;//点赞数
    private int newsIntrNum;//转发数
    //评论,
    private Map<String,Object> map ;

    private Integer newsUserId; //新闻发布人


    private ShowUser showUser;  //用户显示信息
    private String  userName;

    public Integer getNewsCommCount() {
        return newsCommCount;
    }

    public void setNewsCommCount(Integer newsCommCount) {
        this.newsCommCount = newsCommCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getNewsUserId() {
        return newsUserId;
    }

    public void setNewsUserId(Integer newsUserId) {
        this.newsUserId = newsUserId;
    }

    public ShowUser getShowUser() {
        return showUser;
    }

    public void setShowUser(ShowUser showUser) {
        this.showUser = showUser;
    }

    public Integer getIsRelease() {
        return isRelease;
    }

    public void setIsRelease(Integer isRelease) {
        this.isRelease = isRelease;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public String getNewsImageUrl() {
        return newsImageUrl;
    }

    public void setNewsImageUrl(String newsImageUrl) {
        this.newsImageUrl = newsImageUrl;
    }

    public int getNewsLikeNum() {
        return newsLikeNum;
    }

    public void setNewsLikeNum(int newsLikeNum) {
        this.newsLikeNum = newsLikeNum;
    }

    public int getNewsIntrNum() {
        return newsIntrNum;
    }

    public void setNewsIntrNum(int newsIntrNum) {
        this.newsIntrNum = newsIntrNum;
    }

    public NewsType getType() {
        return type;
    }

    public void setType(NewsType type) {
        this.type = type;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }
    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getNewsKey() {
        return newsKey;
    }

    public void setNewsKey(String newsKey) {
        this.newsKey = newsKey;
    }

    public int getClickNumber() {
        return clickNumber;
    }

    public void setClickNumber(int clickNumber) {
        this.clickNumber = clickNumber;
    }

    public String getNwesContent() {
        return nwesContent;
    }

    public void setNwesContent(String nwesContent) {
        this.nwesContent = nwesContent;
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", newsTitle='" + newsTitle + '\'' +
                ", createtime='" + createtime + '\'' +
                ", newsKey='" + newsKey + '\'' +
                ", clickNumber=" + clickNumber +
                ", nwesContent='" + nwesContent + '\'' +
                ", type=" + type +
                ", newsLikeNum=" + newsLikeNum +
                ", newsIntrNum=" + newsIntrNum +
                ", map=" + map +
                ", newsImageUrl='" + newsImageUrl + '\'' +
                ", newsUserId=" + newsUserId +
                ", isRelease=" + isRelease +
                ", userName='" + userName + '\'' +
                '}';
    }
}