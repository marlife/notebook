package com.notebook.entity.pojo;

import com.notebook.entity.vo.user.ShowUser;

public class Proposal {


    private Integer proposalId;

    private String proposalContent;

    private Long proposalTime;

    private boolean isRead;



    public Proposal() {
    }

    public Proposal(String proposalContent) {
        this.proposalContent = proposalContent;
    }

    public Proposal(Integer proposalId, String proposalContent, Long proposalTime, boolean isRead) {
        this.proposalId = proposalId;
        this.proposalContent = proposalContent;
        this.proposalTime = proposalTime;
        this.isRead = isRead;
    }

    public Proposal(String proposalContent, Long proposalTime) {
        this.proposalContent = proposalContent;
        this.proposalTime = proposalTime;
    }

    public Proposal(String proposalContent, Long proposalTime, boolean isRead) {
        this.proposalContent = proposalContent;
        this.proposalTime = proposalTime;
        this.isRead = isRead;
    }

    public boolean getRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public Integer getProposalId() {
        return proposalId;
    }

    public void setProposalId(Integer proposalId) {
        this.proposalId = proposalId;
    }

    public String getProposalContent() {
        return proposalContent;
    }

    public void setProposalContent(String proposalContent) {
        this.proposalContent = proposalContent;
    }

    public Long getProposalTime() {
        return proposalTime;
    }

    public void setProposalTime(Long proposalTime) {
        this.proposalTime = proposalTime;
    }

    @Override
    public String toString() {
        return "Proposal{" +
                "proposalId=" + proposalId +
                ", proposalContent='" + proposalContent + '\'' +
                ", proposalTime=" + proposalTime +
                ", isRead=" + isRead +
                '}';
    }
}