package com.notebook.entity.pojo;

public class Bill {
    private Integer billId;

    private String billType;

    private Double billPirce;

    private String billName;

    private Long createtime;

    private Integer billUserId;

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public Double getBillPirce() {
        return billPirce;
    }

    public void setBillPirce(Double billPirce) {
        this.billPirce = billPirce;
    }

    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Integer getBillUserId() {
        return billUserId;
    }

    public void setBillUserId(Integer billUserId) {
        this.billUserId = billUserId;
    }
}