package com.notebook.entity.pojo;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/12
 */
public class Version {

    private Integer versiond;
    private String  versionNumber;
    private String  versionUrl;
    private long    versionTime;
    private String  versionTitle;
    private String  versionBrief;
    private String  versionContext;

    public Version() {
    }

    public Version(String versionNumber, String versionUrl, long versionTime, String versionTitle, String versionBrief, String versionContext) {
        this.versionNumber = versionNumber;
        this.versionUrl = versionUrl;
        this.versionTime = versionTime;
        this.versionTitle = versionTitle;
        this.versionBrief = versionBrief;
        this.versionContext = versionContext;
    }

    public String getVersionTitle() {
        return versionTitle;
    }

    public void setVersionTitle(String versionTitle) {
        this.versionTitle = versionTitle;
    }

    public Integer getVersiond() {
        return versiond;
    }

    public void setVersiond(Integer versiond) {
        this.versiond = versiond;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getVersionUrl() {
        return versionUrl;
    }

    public void setVersionUrl(String versionUrl) {
        this.versionUrl = versionUrl;
    }

    public String getVersionBrief() {
        return versionBrief;
    }

    public void setVersionBrief(String versionBrief) {
        this.versionBrief= versionBrief;
    }

    public String getVersionContext() {
        return versionContext;
    }

    public void setVersionContext(String versionContext) {
        this.versionContext = versionContext;
    }

    public long getVersionTime() {
        return versionTime;
    }

    public void setVersionTime(long versionTime) {
        this.versionTime = versionTime;
    }

    @Override
    public String toString() {
        return "Version{" +
                "versiond=" + versiond +
                ", versionNumber='" + versionNumber + '\'' +
                ", versionUrl='" + versionUrl + '\'' +
                ", versionTitle='" + versionTitle + '\'' +
                ", versionBrief='" + versionBrief + '\'' +
                ", versionContext='" + versionContext + '\'' +
                ", versionTime=" + versionTime +
                '}';
    }
}
