package com.notebook.entity.pojo;

import com.notebook.entity.vo.news.NewsCommUserVO;

import java.util.Map;

public class NewsComm {
    private Integer commId;
    private String commContent;
    private Long commTime;
    private Integer commNewsId;
    private Integer commUserId;
    private Integer commLikeNum;
    private NewsCommUserVO newsCommUserVO;

    public NewsComm(Integer commId, String commContent, Long commTime, Integer commNewsId, Integer commUserId, Integer commLikeNum, NewsCommUserVO newsCommUserVO) {
        this.commId = commId;
        this.commContent = commContent;
        this.commTime = commTime;
        this.commNewsId = commNewsId;
        this.commUserId = commUserId;
        this.commLikeNum = commLikeNum;
        this.newsCommUserVO = newsCommUserVO;
    }

    public NewsComm() {
    }

    public NewsComm(String commContent, Long commTime, Integer commNewsId, Integer commUserId) {
        this.commContent = commContent;
        this.commTime = commTime;
        this.commNewsId = commNewsId;
        this.commUserId = commUserId;
    }

    public Integer getCommId() {
        return commId;
    }

    public void setCommId(Integer commId) {
        this.commId = commId;
    }

    public String getCommContent() {
        return commContent;
    }

    public void setCommContent(String commContent) {
        this.commContent = commContent;
    }

    public Long getCommTime() {
        return commTime;
    }

    public void setCommTime(Long commTime) {
        this.commTime = commTime;
    }

    public Integer getCommNewsId() {
        return commNewsId;
    }

    public void setCommNewsId(Integer commNewsId) {
        this.commNewsId = commNewsId;
    }

    public Integer getCommUserId() {
        return commUserId;
    }

    public void setCommUserId(Integer commUserId) {
        this.commUserId = commUserId;
    }

    public Integer getCommLikeNum() {
        return commLikeNum;
    }

    public void setCommLikeNum(Integer commLikeNum) {
        this.commLikeNum = commLikeNum;
    }

    public NewsCommUserVO getNewsCommUserVO() {
        return newsCommUserVO;
    }

    public void setNewsCommUserVO(NewsCommUserVO newsCommUserVO) {
        this.newsCommUserVO = newsCommUserVO;
    }

    @Override
    public String toString() {
        return "NewsComm{" +
                "commId=" + commId +
                ", commContent='" + commContent + '\'' +
                ", commTime=" + commTime +
                ", commNewsId=" + commNewsId +
                ", commUserId=" + commUserId +
                ", commLikeNum=" + commLikeNum +
                ", newsCommUserVO=" + newsCommUserVO +
                '}';
    }
}