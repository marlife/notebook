package com.notebook.entity.pojo;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/5
 */
public class NewsLike {

    private Integer likeId;
    private Integer likeNewsId;
    private Integer likeUserId;
    private String deviceId;

    public NewsLike() {
    }

    public NewsLike(Integer likeNewsId, String deviceId) {
        this.likeNewsId = likeNewsId;
        this.deviceId = deviceId;
    }

    public NewsLike(Integer likeId, Integer likeNewsId, String deviceId) {
        this.likeId = likeId;
        this.likeNewsId = likeNewsId;
        this.deviceId = deviceId;
    }

    public Integer getLikeId() {
        return likeId;
    }

    public void setLikeId(Integer likeId) {
        this.likeId = likeId;
    }

    public Integer getLikeNewsId() {
        return likeNewsId;
    }

    public void setLikeNewsId(Integer likeNewsId) {
        this.likeNewsId = likeNewsId;
    }

    public Integer getLikeUserId() {
        return likeUserId;
    }

    public void setLikeUserId(Integer likeUserId) {
        this.likeUserId = likeUserId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
