package com.notebook.entity.pojo;

public class Favorite {
    private Integer favoId;

    private Integer favoUserId;

    private Integer favoNewsId;

    private long favoTime;

    public Favorite(Integer favoId, Integer favoUserId, Integer favoNewsId, long favoTime) {
        this.favoId = favoId;
        this.favoUserId = favoUserId;
        this.favoNewsId = favoNewsId;
        this.favoTime = favoTime;
    }

    public Favorite(Integer favoUserId, Integer favoNewsId, long favoTime) {
        this.favoUserId = favoUserId;
        this.favoNewsId = favoNewsId;
        this.favoTime = favoTime;
    }

    public Favorite(Integer favoUserId, Integer favoNewsId) {
        this.favoUserId = favoUserId;
        this.favoNewsId = favoNewsId;
    }

    public Favorite() {
    }

    public Integer getFavoId() {

        return favoId;
    }

    public void setFavoId(Integer favoId) {
        this.favoId = favoId;
    }

    public Integer getFavoUserId() {
        return favoUserId;
    }

    public void setFavoUserId(Integer favoUserId) {
        this.favoUserId = favoUserId;
    }

    public Integer getFavoNewsId() {
        return favoNewsId;
    }

    public void setFavoNewsId(Integer favoNewsId) {
        this.favoNewsId = favoNewsId;
    }

    public long getFavoTime() {
        return favoTime;
    }

    public void setFavoTime(long favoTime) {
        this.favoTime = favoTime;
    }

    @Override
    public String toString() {
        return "Favorite{" +
                "favoId=" + favoId +
                ", favoUserId=" + favoUserId +
                ", favoNewsId=" + favoNewsId +
                ", favoTime=" + favoTime +
                '}';
    }
}