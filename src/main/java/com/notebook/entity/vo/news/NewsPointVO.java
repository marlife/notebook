package com.notebook.entity.vo.news;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/3
 */
public class NewsPointVO {
    private Integer newsId;
    private String newsTitle;
    private Integer newsLikeNum;

    public NewsPointVO(Integer newsId, String newsTitle, Integer newsLikeNum) {
        this.newsId = newsId;
        this.newsTitle = newsTitle;
        this.newsLikeNum = newsLikeNum;
    }

    public NewsPointVO() {
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public Integer getNewsLikeNum() {
        return newsLikeNum;
    }

    public void setNewsLikeNum(Integer newsLikeNum) {
        this.newsLikeNum = newsLikeNum;
    }

    @Override
    public String toString() {
        return "NewsPointVO{" +
                "newsId=" + newsId +
                ", newsTitle='" + newsTitle + '\'' +
                ", newsLikeNum=" + newsLikeNum +
                '}';
    }
}
