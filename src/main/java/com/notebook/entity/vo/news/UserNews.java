package com.notebook.entity.vo.news;

import com.notebook.entity.vo.user.ShowUser;

public class UserNews {


    private Integer newsId; //新闻编号;

    private String newsTitle;// 新闻标题;

    private String newsContent; //新闻内容;

    private Integer newsTypeId;// 新闻类型编号;

    private Integer newsUserId;   //新闻发布人;

    private Integer isRelease;  // 是否发布;

    private Long createTime; // 新闻发布时间;

    private String newsKey;//新闻关键字

    private String NewsTypeName; //文章分类名称

    private Integer likeNum;   //点赞数

    private Integer commNum;   //评论数

    private String newsPic;  //主页的显示图片

    private Integer newsIntrNum; //转发数

    private ShowUser showUser;//当前文章的用户

    public ShowUser getShowUser() {
        return showUser;
    }

    public void setShowUser(ShowUser showUser) {
        this.showUser = showUser;
    }

    public Integer getNewsIntrNum() {
        return newsIntrNum;
    }

    public void setNewsIntrNum(Integer newsIntrNum) {
        this.newsIntrNum = newsIntrNum;
    }

    public String getNewsPic() {
        return newsPic;
    }

    public void setNewsPic(String newsPic) {
        this.newsPic = newsPic;
    }

    public Integer getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(Integer likeNum) {
        this.likeNum = likeNum;
    }

    public Integer getCommNum() {
        return commNum;
    }

    public void setCommNum(Integer commNum) {
        this.commNum = commNum;
    }

    public String getNewsTypeName() {
        return NewsTypeName;
    }

    public void setNewsTypeName(String newsTypeName) {
        NewsTypeName = newsTypeName;
    }

    public UserNews() {
    }

    public UserNews(String newsTitle, String newsContent, Integer newsTypeId, Integer newsUserId, Long createTime, String newsKey) {
        this.newsTitle = newsTitle;
        this.newsContent = newsContent;
        this.newsTypeId = newsTypeId;
        this.newsUserId = newsUserId;
        this.createTime = createTime;
        this.newsKey = newsKey;
    }

    public UserNews(String newsTitle, String newsContent, Integer newsTypeId, Integer newsUserId, Integer isRelease, Long createTime, String newsKey) {
        this.newsTitle = newsTitle;
        this.newsContent = newsContent;
        this.newsTypeId = newsTypeId;
        this.newsUserId = newsUserId;
        this.isRelease = isRelease;
        this.createTime = createTime;
        this.newsKey = newsKey;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    public Integer getNewsTypeId() {
        return newsTypeId;
    }

    public void setNewsTypeId(Integer newsTypeId) {
        this.newsTypeId = newsTypeId;
    }

    public Integer getNewsUserId() {
        return newsUserId;
    }

    public void setNewsUserId(Integer newsUserId) {
        this.newsUserId = newsUserId;
    }

    public Integer getRelease() {
        return isRelease;
    }

    public void setRelease(Integer release) {
        isRelease = release;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getNewsKey() {
        return newsKey;
    }

    public void setNewsKey(String newsKey) {
        this.newsKey = newsKey;
    }

    @Override
    public String toString() {
        return "UserNews{" +
                "newsId=" + newsId +
                ", newsTitle='" + newsTitle + '\'' +
                ", newsContent='" + newsContent + '\'' +
                ", newsTypeId=" + newsTypeId +
                ", newsUserId=" + newsUserId +
                ", isRelease=" + isRelease +
                ", createTime=" + createTime +
                ", newsKey='" + newsKey + '\'' +
                ", NewsTypeName='" + NewsTypeName + '\'' +
                ", likeNum=" + likeNum +
                ", commNum=" + commNum +
                ", newsPic='" + newsPic + '\'' +
                ", newsIntrNum=" + newsIntrNum +
                ", showUser=" + showUser +
                '}';
    }
}
