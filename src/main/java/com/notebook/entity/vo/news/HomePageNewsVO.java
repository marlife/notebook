package com.notebook.entity.vo.news;

import com.notebook.entity.pojo.NewsType;
import com.notebook.entity.vo.user.ShowUser;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/2
 */
public class HomePageNewsVO {
    private Integer newsId;
    private String newsTitle;
    private String createtime;
    private String newsPic;
    private String typeName;
    private int newsLikeNum;//点赞数
    private int newsIntrNum;//转发数
    private Integer commCount;
    private Integer newsUserId;  //新闻发布人
    private ShowUser showUser;
    public HomePageNewsVO(Integer newsId, String newsTitle, String createtime, String newsPic, String typeName, int newsLikeNum, int newsIntrNum) {
        this.newsId = newsId;
        this.newsTitle = newsTitle;
        this.createtime = createtime;
        this.newsPic = newsPic;
        this.typeName = typeName;
        this.newsLikeNum = newsLikeNum;
        this.newsIntrNum = newsIntrNum;
    }

    public Integer getNewsUserId() {
        return newsUserId;
    }

    public void setNewsUserId(Integer newsUserId) {
        this.newsUserId = newsUserId;
    }

    public ShowUser getShowUser() {
        return showUser;
    }

    public void setShowUser(ShowUser showUser) {
        this.showUser = showUser;
    }

    public HomePageNewsVO(Integer newsId, String newsTitle, String createtime, String newsPic, String typeName, int newsLikeNum, int newsIntrNum, Integer commCount) {
        this.newsId = newsId;
        this.newsTitle = newsTitle;
        this.createtime = createtime;
        this.newsPic = newsPic;
        this.typeName = typeName;
        this.newsLikeNum = newsLikeNum;
        this.newsIntrNum = newsIntrNum;
        this.commCount = commCount;
    }

    public HomePageNewsVO() {
    }

    @Override
    public String toString() {
        return "HomePageNewsVO{" +
                "newsId=" + newsId +
                ", newsTitle='" + newsTitle + '\'' +
                ", createtime='" + createtime + '\'' +
                ", newsPic='" + newsPic + '\'' +
                ", typeName='" + typeName + '\'' +
                ", newsLikeNum=" + newsLikeNum +
                ", newsIntrNum=" + newsIntrNum +
                ", commCount=" + commCount +
                ", newsUserId=" + newsUserId +
                ", showUser=" + showUser +
                '}';
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getNewsPic() {
        return newsPic;
    }

    public void setNewsPic(String newsPic) {
        this.newsPic = newsPic;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getNewsLikeNum() {
        return newsLikeNum;
    }

    public void setNewsLikeNum(int newsLikeNum) {
        this.newsLikeNum = newsLikeNum;
    }

    public int getNewsIntrNum() {
        return newsIntrNum;
    }

    public void setNewsIntrNum(int newsIntrNum) {
        this.newsIntrNum = newsIntrNum;
    }

    public Integer getCommCount() {
        return commCount;
    }

    public void setCommCount(Integer commCount) {
        this.commCount = commCount;
    }

}
