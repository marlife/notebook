package com.notebook.entity.vo.news;

import com.notebook.entity.pojo.NewsType;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/19
 */
public class GetNewsVO {
    private String title;
    private String content;
    private String NewsType;
    private String time;
    private String key;

    public GetNewsVO(String title, String content, String newsType, String time, String key) {
        this.title = title;
        this.content = content;
        NewsType = newsType;
        this.time = time;
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNewsType() {
        return NewsType;
    }

    public void setNewsType(String newsType) {
        NewsType = newsType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
