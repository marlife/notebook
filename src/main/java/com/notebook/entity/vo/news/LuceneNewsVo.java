package com.notebook.entity.vo.news;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/10
 */
public class LuceneNewsVo {

    private Integer newsId;
    private String title;
    private String newsKey;

    public LuceneNewsVo(Integer newsId, String title) {
        this.newsId = newsId;
        this.title = title;
    }

    public String getNewsKey() {
        return newsKey;
    }

    public void setNewsKey(String newsKey) {
        this.newsKey = newsKey;
    }



    public LuceneNewsVo() {
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "LuceneNewsVo{" +
                "newsId=" + newsId +
                ", title='" + title + '\'' +
                ", newsKey='" + newsKey + '\'' +
                '}';
    }
}
