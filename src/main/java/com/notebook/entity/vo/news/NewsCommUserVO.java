package com.notebook.entity.vo.news;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/8
 */
public class NewsCommUserVO {

    private Integer commUserId;
    private String commUserName;
    private String commUserPic;

    public NewsCommUserVO(Integer commUserId, String commUserName, String commUserPic) {
        this.commUserId = commUserId;
        this.commUserName = commUserName;
        this.commUserPic = commUserPic;
    }

    public Integer getCommUserId() {
        return commUserId;
    }

    public void setCommUserId(Integer commUserId) {
        this.commUserId = commUserId;
    }

    public String getCommUserName() {
        return commUserName;
    }

    public void setCommUserName(String commUserName) {
        this.commUserName = commUserName;
    }

    public String getCommUserPic() {
        return commUserPic;
    }

    public void setCommUserPic(String commUserPic) {
        this.commUserPic = commUserPic;
    }

    public NewsCommUserVO() {
    }

    @Override
    public String toString() {
        return "NewsCommUserVO{" +
                "commUserId=" + commUserId +
                ", commUserName='" + commUserName + '\'' +
                ", commUserPic='" + commUserPic + '\'' +
                '}';
    }
}


