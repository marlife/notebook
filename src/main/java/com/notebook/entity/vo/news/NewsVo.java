package com.notebook.entity.vo.news;

import com.notebook.entity.pojo.NewsType;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/20
 */
public class NewsVo {
    private Integer newsId;
    private String  newsTitle;
    private String  newsKey;
    private String  newsContent;
    private Integer newsTypeId;
    private long    createTime;
    private Integer isRelease;
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIsRelease() {
        return isRelease;
    }

    public void setIsRelease(Integer isRelease) {
        this.isRelease = isRelease;
    }

    @Override
    public String toString() {
        return "NewsVo{" +
                "newsId=" + newsId +
                ", newsTitle='" + newsTitle + '\'' +
                ", newsKey='" + newsKey + '\'' +
                ", newsContent='" + newsContent + '\'' +
                ", newsTypeId=" + newsTypeId +
                ", createTime=" + createTime +
                '}';
    }

    public NewsVo(Integer newsId, String newsTitle, String newsKey, String newsContent, Integer newsTypeId ) {
        this.newsId = newsId;
        this.newsTitle = newsTitle;
        this.newsKey = newsKey;
        this.newsContent = newsContent;
        this.newsTypeId = newsTypeId;

    }

    public NewsVo(String newsTitle, String newsKey, String newsContent, Integer newsTypeId, long createTime) {
        this.newsTitle = newsTitle;
        this.newsKey = newsKey;
        this.newsContent = newsContent;
        this.newsTypeId = newsTypeId;
        this.createTime = createTime;
    }


    public NewsVo() {
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsKey() {
        return newsKey;
    }

    public void setNewsKey(String newsKey) {
        this.newsKey = newsKey;
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    public Integer getNewsTypeId() {
        return newsTypeId;
    }

    public void setNewsTypeId(Integer newsTypeId) {
        this.newsTypeId = newsTypeId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
}
