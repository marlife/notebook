package com.notebook.entity.vo.news;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/10
 */
public class CarouselNewsVo {

    private Integer id;
    private String  title;
    private String  newsPic;

    public CarouselNewsVo(Integer id, String title, String newsPic) {
        this.id = id;
        this.title = title;
        this.newsPic = newsPic;
    }

    public CarouselNewsVo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNewsPic() {
        return newsPic;
    }

    public void setNewsPic(String newsPic) {
        this.newsPic = newsPic;
    }

    @Override
    public String toString() {
        return "CarouselNewsVo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", newsPic='" + newsPic + '\'' +
                '}';
    }
}
