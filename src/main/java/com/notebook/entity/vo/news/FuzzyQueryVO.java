package com.notebook.entity.vo.news;

import java.util.Date;

/**
 * 文件描述:  新闻的值对象  用于实现模糊查询
 * 创建用户:emotion
 * 创建时间:2018/3/15
 */
public class FuzzyQueryVO {
    private Integer typeId;
    private long startTime;
    private long endTime;
    private String newsTitle;

    @Override
    public String toString() {
        return "FuzzyQueryVO{" +
                "typeId=" + typeId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", newsTitle='" + newsTitle + '\'' +
                '}';
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public FuzzyQueryVO(Integer typeId, long startTime, long endTime, String newsTitle) {

        this.typeId = typeId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.newsTitle = newsTitle;
    }

    public FuzzyQueryVO() {
    }
}
