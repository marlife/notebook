package com.notebook.entity.vo.news;

import com.notebook.entity.pojo.NewsType;
import com.notebook.entity.vo.user.ShowUser;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/4
 */
public class NewsByTypeVO {
    private Integer newsId;
    private String newsTitle;
    private String createtime;
    private String newsKey;
    private int clickNumber;
    private String nwesContent;
    private NewsType type;
    private int newsLikeNum;//点赞数
    private int newsIntrNum;//转发数
    private Integer newsUserId;
    private String newsImageUrl;//新闻图片地址
    private Integer newsRankNum;//新闻评论数量
    private ShowUser showUser;
    public NewsByTypeVO() {
    }

    public Integer getNewsUserId() {
        return newsUserId;
    }

    public void setNewsUserId(Integer newsUserId) {
        this.newsUserId = newsUserId;
    }

    public ShowUser getShowUser() {
        return showUser;
    }

    public void setShowUser(ShowUser showUser) {
        this.showUser = showUser;
    }

    public Integer getNewsRankNum() {
        return newsRankNum;
    }

    public void setNewsRankNum(Integer newsRankNum) {
        this.newsRankNum = newsRankNum;
    }

    public String getNewsImageUrl() {
        return newsImageUrl;
    }

    public void setNewsImageUrl(String newsImageUrl) {
        this.newsImageUrl = newsImageUrl;
    }

    public int getNewsLikeNum() {
        return newsLikeNum;
    }

    public void setNewsLikeNum(int newsLikeNum) {
        this.newsLikeNum = newsLikeNum;
    }

    public int getNewsIntrNum() {
        return newsIntrNum;
    }

    public void setNewsIntrNum(int newsIntrNum) {
        this.newsIntrNum = newsIntrNum;
    }

    public NewsType getType() {
        return type;
    }

    public void setType(NewsType type) {
        this.type = type;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }
    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getNewsKey() {
        return newsKey;
    }

    public void setNewsKey(String newsKey) {
        this.newsKey = newsKey;
    }

    public int getClickNumber() {
        return clickNumber;
    }

    public void setClickNumber(int clickNumber) {
        this.clickNumber = clickNumber;
    }

    public String getNwesContent() {
        return nwesContent;
    }

    public void setNwesContent(String nwesContent) {
        this.nwesContent = nwesContent;
    }

    @Override
    public String toString() {
        return "NewsByTypeVO{" +
                "newsId=" + newsId +
                ", newsTitle='" + newsTitle + '\'' +
                ", createtime='" + createtime + '\'' +
                ", newsKey='" + newsKey + '\'' +
                ", clickNumber=" + clickNumber +
                ", nwesContent='" + nwesContent + '\'' +
                ", type=" + type +
                ", newsLikeNum=" + newsLikeNum +
                ", newsIntrNum=" + newsIntrNum +
                ", newsUserId=" + newsUserId +
                ", newsImageUrl='" + newsImageUrl + '\'' +
                ", newsRankNum=" + newsRankNum +
                ", showUser=" + showUser +
                '}';
    }
}
