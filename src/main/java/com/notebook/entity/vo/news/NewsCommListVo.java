package com.notebook.entity.vo.news;

public class NewsCommListVo {
    private Integer commId;
    private String commContent;
    private long commTime;

    private Integer commUserId;
    private Integer commLikeNum;

    private String newsTitle;
    private String userName;
    private String userPic;

    public NewsCommListVo() {
    }

    public Integer getCommId() {
        return commId;
    }

    public void setCommId(Integer commId) {
        this.commId = commId;
    }

    public String getCommContent() {
        return commContent;
    }

    public void setCommContent(String commContent) {
        this.commContent = commContent;
    }

    public long getCommTime() {
        return commTime;
    }

    public void setCommTime(long commTime) {
        this.commTime = commTime;
    }

    public Integer getCommUserId() {
        return commUserId;
    }

    public void setCommUserId(Integer commUserId) {
        this.commUserId = commUserId;
    }

    public Integer getCommLikeNum() {
        return commLikeNum;
    }

    public void setCommLikeNum(Integer commLikeNum) {
        this.commLikeNum = commLikeNum;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }
}
