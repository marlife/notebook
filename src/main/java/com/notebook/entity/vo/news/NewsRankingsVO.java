package com.notebook.entity.vo.news;

import com.notebook.entity.pojo.NewsComm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/3
 */
public class NewsRankingsVO {
    private  Integer newsId;
    private  String newsTitle;
    private  Integer rankingCount;

    @Override
    public String toString() {
        return "NewsRankingsVO{" +
                "newsId=" + newsId +
                ", newsTitle='" + newsTitle + '\'' +
                ", rankingCount=" + rankingCount +
                '}';
    }

    public NewsRankingsVO(Integer newsId, String newsTitle, Integer rankingCount) {
        this.newsId = newsId;
        this.newsTitle = newsTitle;
        this.rankingCount = rankingCount;
    }

    public NewsRankingsVO() {
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public Integer getRankingCount() {
        return rankingCount;
    }

    public void setRankingCount(Integer rankingCount) {
        this.rankingCount = rankingCount;
    }
}
