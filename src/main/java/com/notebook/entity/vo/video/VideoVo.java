package com.notebook.entity.vo.video;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/6/5
 */
public class VideoVo {
    private Integer videoId;
    private String videoTitle;
    private String videoCover;

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoCover() {
        return videoCover;
    }

    public void setVideoCover(String videoCover) {
        this.videoCover = videoCover;
    }

    @Override
    public String toString() {
        return "VideoVo{" +
                "videoId=" + videoId +
                ", videoTitle='" + videoTitle + '\'' +
                ", videoCover='" + videoCover + '\'' +
                '}';
    }
}
