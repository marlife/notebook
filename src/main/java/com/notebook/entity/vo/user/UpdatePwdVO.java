package com.notebook.entity.vo.user;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/31
 */
public class UpdatePwdVO {

    private String phone;
    private String password;

    public UpdatePwdVO(String phone, String password) {
        this.phone = phone;
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
