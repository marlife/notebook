package com.notebook.entity.vo.user;


public class CorporateUser {

    private String user_name;
    private String user_phone;
    private String user_emali;
    private String user_head_pic;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_emali() {
        return user_emali;
    }

    public void setUser_emali(String user_emali) {
        this.user_emali = user_emali;
    }

    public String getUser_head_pic() {
        return user_head_pic;
    }

    public void setUser_head_pic(String user_head_pic) {
        this.user_head_pic = user_head_pic;
    }

    @Override
    public String toString() {
        return "CorporateUser{" +
                "user_name='" + user_name + '\'' +
                ", user_phone='" + user_phone + '\'' +
                ", user_emali='" + user_emali + '\'' +
                ", user_head_pic='" + user_head_pic + '\'' +
                '}';
    }
}
