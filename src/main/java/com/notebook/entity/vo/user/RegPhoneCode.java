package com.notebook.entity.vo.user;

public class RegPhoneCode {
    private String phone;
    private Integer code;
    private String time;
    public RegPhoneCode(String phone, Integer code, String time) {
        this.phone = phone;
        this.code = code;
        this.time = time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
