package com.notebook.entity.vo.user;

/**
 * 文件描述: 用户的值对象 用于用户登录
 * 创建用户:emotion
 * 创建时间:2018/3/11
 */
public class LoginUserVO {

    private int id;
    private String passWord;
    private String phone;
    private boolean accountStatus;


    public LoginUserVO(String passWord, String phone) {

        this.passWord = passWord;
        this.phone = phone;

    }

    public LoginUserVO(String passWord, String phone, boolean accountStatus) {
        this.passWord = passWord;
        this.phone = phone;
        this.accountStatus = accountStatus;
    }

    public LoginUserVO(int id, String passWord, String phone, boolean accountStatus) {
        this.id = id;
        this.passWord = passWord;
        this.phone = phone;
        this.accountStatus = accountStatus;
    }

    public LoginUserVO() {
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(boolean accountStatus) {
        this.accountStatus = accountStatus;
    }
}
