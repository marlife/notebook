package com.notebook.entity.vo.user;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/17
 */
public class AdminRegVo {
    private String userName;
    private String phone;
    private String password;
    private boolean sex;
    private String email;
    private String userAutograph;
    private long regTime;

    public AdminRegVo(String userName, String phone, String password, boolean sex, String email, String userAutograph, long regTime) {
        this.userName = userName;
        this.phone = phone;
        this.password = password;
        this.sex = sex;
        this.email = email;
        this.userAutograph = userAutograph;
        this.regTime = regTime;
    }

    public AdminRegVo(String userName, String phone, boolean sex, String email, String userAutograph) {
        this.userName = userName;
        this.phone = phone;
        this.sex = sex;
        this.email = email;
        this.userAutograph = userAutograph;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public AdminRegVo() {
    }

    public long getRegTime() {
        return regTime;
    }

    public void setRegTime(long regTime) {
        this.regTime = regTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserAutograph() {
        return userAutograph;
    }

    public void setUserAutograph(String userAutograph) {
        this.userAutograph = userAutograph;
    }
}
