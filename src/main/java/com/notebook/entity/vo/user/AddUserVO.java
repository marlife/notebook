package com.notebook.entity.vo.user;

/**
 * 文件描述:用户的值对象 用于用户注册
 * 创建用户:emotion
 * 创建时间:2018/3/11
 */
public class AddUserVO {
    private int id;
    private String userName;
    private String password;
    private String phone;
    private long   regTime;
    private String userEmail;
    private Boolean userPower;
    private String userPic;

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public Boolean getUserPower() {
        return userPower;
    }

    public void setUserPower(Boolean userPower) {
        this.userPower = userPower;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public AddUserVO() {
    }

    public AddUserVO(String password, String phone, long regTime) {
        this.password = password;
        this.phone = phone;
        this.regTime = regTime;
    }

    public AddUserVO(String userName, String password, String phone, long RegTime) {
        this.userName=userName;
        this.password = password;
        this.phone = phone;
        this.regTime=RegTime;
    }

    public AddUserVO(int id, String password, String phone) {
        this.id = id;
        this.password = password;
        this.phone = phone;
    }

    public long getRegTime() {
        return regTime;
    }

    public void setRegTime(long regTime) {
        this.regTime = regTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
