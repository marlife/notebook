package com.notebook.entity.vo.user;


public class ShowUser {

    private Integer userId;

    private String  userName;

    private String  userPic;

    private String newsCount;

    public String getNewsCount() {
        return newsCount;
    }

    public void setNewsCount(String newsCount) {
        this.newsCount = newsCount;
    }

    public ShowUser(Integer userId, String userName, String userPic) {
        this.userId = userId;
        this.userName = userName;
        this.userPic = userPic;
    }

    public ShowUser() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    @Override
    public String toString() {
        return "ShowUser{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userPic='" + userPic + '\'' +
                ", newsCount='" + newsCount + '\'' +
                '}';
    }
}
