package com.notebook.entity.vo.user;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/13
 */
public class PhoneCodeVo {

    private String count;
    private String accountid;
    private Integer CodeNum;
    private String content;
    public PhoneCodeVo() {
    }

    public PhoneCodeVo(String count, String accountid, Integer codeNum, String content) {
        this.count = count;
        this.accountid = accountid;
        CodeNum = codeNum;
        this.content = content;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public Integer getCodeNum() {
        return CodeNum;
    }

    public void setCodeNum(Integer codeNum) {
        CodeNum = codeNum;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        content = content;
    }
}
