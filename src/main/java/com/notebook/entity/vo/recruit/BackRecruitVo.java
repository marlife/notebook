package com.notebook.entity.vo.recruit;


public class BackRecruitVo {
    private String  corporate_name;     //公司名称
    private Integer corporate_id;       //公司id
    private Integer user_id;            //用户id
    private String  user_name;          //用户名称
    private String  user_head_pic;      //用户头像
    private Long    createtime;         //发布时间
    private Integer recruit_id;         //招聘id
    private String  post_name;         //招聘岗位

    public String getCorporate_name() {
        return corporate_name;
    }

    public void setCorporate_name(String corporate_name) {
        this.corporate_name = corporate_name;
    }

    public Integer getCorporate_id() {
        return corporate_id;
    }

    public void setCorporate_id(Integer corporate_id) {
        this.corporate_id = corporate_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_head_pic() {
        return user_head_pic;
    }

    public void setUser_head_pic(String user_head_pic) {
        this.user_head_pic = user_head_pic;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Integer getRecruit_id() {
        return recruit_id;
    }

    public void setRecruit_id(Integer recruit_id) {
        this.recruit_id = recruit_id;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    @Override
    public String toString() {
        return "BackRecruitVo{" +
                "corporate_name='" + corporate_name + '\'' +
                ", corporate_id=" + corporate_id +
                ", user_id=" + user_id +
                ", user_name='" + user_name + '\'' +
                ", user_head_pic='" + user_head_pic + '\'' +
                ", createtime=" + createtime +
                ", recruit_id=" + recruit_id +
                ", post_name='" + post_name + '\'' +
                '}';
    }
}
