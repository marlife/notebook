package com.notebook.dao;

import com.notebook.entity.pojo.Note;

public interface INoteDao {
    int deleteByPrimaryKey(Integer noteId);
    //按用户id删除收藏
    int deleteUserByPrimaryKey(Integer userId);

    int insert(Note record);

    int insertSelective(Note record);

    Note selectByPrimaryKey(Integer noteId);

    int updateByPrimaryKeySelective(Note record);

    int updateByPrimaryKeyWithBLOBs(Note record);

    int updateByPrimaryKey(Note record);
}