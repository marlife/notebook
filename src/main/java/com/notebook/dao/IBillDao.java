package com.notebook.dao;

import com.notebook.entity.pojo.Bill;

public interface IBillDao {

    int insert(Bill record);

    int insertSelective(Bill record);

    Bill selectByPrimaryKey(Integer billId);

    int updateByPrimaryKeySelective(Bill record);

    int updateByPrimaryKey(Bill record);

    int deleteByPrimaryKey(Integer userId);

}