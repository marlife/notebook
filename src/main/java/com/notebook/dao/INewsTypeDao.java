package com.notebook.dao;

import com.notebook.entity.pojo.NewsType;

import java.util.List;

public interface INewsTypeDao {
    //查询全部新闻分类
    List<NewsType> queryAll();
    //添加新闻
    int insert(String newsTypeName);
    //根据id删除新闻
    int deleteByPrimaryKey(Integer newsTypeId);
    //修改新闻分类
    int updateByPrimaryKey(NewsType newsType);

}