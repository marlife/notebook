package com.notebook.dao;

import com.notebook.entity.pojo.User;
import com.notebook.entity.vo.user.*;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.INTERNAL;

import java.util.List;
import java.util.Map;

public interface IUserDao {

    int insert(User record);

    int insertSelective(User record);



    int updateByPrimaryKeySelective(User record);



     //根据id查询用户信息;
     User selectByPrimaryKey(Integer userId);
     //查询所有用户
     List<User> userList();

    //按用户id删除用户
    int deleteByPrimaryKey(Integer userId);
    //用户注册 参数是:用户注册的值对象
    int UserReg(AddUserVO userVO);
    //用户注册检测手机号是否存在
    Integer inspectPhone(String phone);
    //用户登录
    List<LoginUserVO> UserLogin(LoginUserVO userVO);
    //用户找回密码
    int BackPassword(LoginUserVO userVO);
    //根据手机号查询用户信息
    User FindByPhone(String phone);
    //用户修改个人信息
    int UpdateFindByid(UpdateInfoVO record);
    //根据账号修改密码
    int updatePassoWord(UpdatePwdVO pwdVO);
    //查询所有管理員
    List<User> adminList();
    //添加管理员
    Integer addAdmin(AdminRegVo regVo);
    //修改信息
    Integer UpdateInfo(AdminRegVo regVo);
    //获取用户显示的信息
    ShowUser getUserInfo(Integer userid);
    //修改用户头像
    Integer updateUserPic(@Param("pic")String pic,@Param("userId") Integer userId);

    //管理员登陆
    User AdminLogin(String phone);
    //根据id查询用户信息
    User FindById(Integer userId);
    //后台更新信息
    Integer updateUserInfo(Map<String,Object> map);
    //新增用户
    Integer addUserInfo(AddUserVO addUserVO);

    CorporateUser getCorporateUserInfo(Integer id);
}