package com.notebook.dao;

import com.notebook.entity.pojo.Favorite;
import com.notebook.entity.vo.news.FavoriteNewsVO;

import java.util.List;

public interface IFavoriteDao {



    //按新闻id删除新闻
    int deleteByPrimaryKey(Integer newsId);
    //按用户id删除收藏
    int deleteUserByPrimaryKey(Integer userId);


    int insertSelective(Favorite record);

    Favorite selectByPrimaryKey(Integer favoId);

    int updateByPrimaryKeySelective(Favorite record);

    int updateByPrimaryKey(Favorite record);
    //设置收藏
    int insert(Favorite record);
    //查询这条新闻这个用户是否收藏
    Integer getYesNoFavo(Favorite favorite);
    //根据主键id删除此数据
    Integer deleteById(Integer id);
    //获取共累计收藏数量
    Integer getFavoriteCount();
    //根据用户id查询全部的新闻id
    List<Integer> findByUserIdGetNewsId(Integer userId);
}