package com.notebook.dao;

import com.notebook.entity.pojo.Version;

import java.util.List;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/12
 */
public interface IVersionDao {

    Version checkVersion(String versionNum);
    List<Version> queryAll();
    Version findById(Integer versionId);
    String findByVersionNum();
    Integer checkVersionNum(String versionNum);
    Integer addVersion(Version version);
}
