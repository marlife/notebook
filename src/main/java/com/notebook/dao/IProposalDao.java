package com.notebook.dao;

import com.notebook.entity.pojo.Proposal;

import java.util.List;

public interface IProposalDao {


    //新增建议
    int insert(Proposal record);
    //查询全部建议
    List<Proposal> queryAll();

    //获取未读的信息数量
    Integer GetUnreadInfoCount();

    //根据id删除留言
    Integer delById(Integer id);
    //根据id设置已读
    Integer settingRead(Integer id);

    //获取未读的留言内容
    List<Proposal> getNoReadList();
}