package com.notebook.utils;

import java.util.Random;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/13
 */
public class NumberUtil {



    //获取一个6位的随机码
    public static Integer getRandomSixNum(){
        return (int)((Math.random()*9+1)*100000);
    }
    //获取10以内的随机数
    public static Integer getRandom10Num(){
        return (int)((Math.random()*9+1));
    }
}
