package com.notebook.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 文件描述: 处理密码加密
 * 创建用户:emotion
 * 创建时间:2018/3/10
 */
public class EncryptUtil {

    /**
     * 密码加密
     * @param value  传入的密码字符串
     * @return  32位的密码
     */
    public static String md5Util(String value){
        return DigestUtils.md5Hex(value);
    }



}
