package com.notebook.utils;

import com.notebook.entity.vo.user.PhoneCodeVo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/4/13
 */
public class SendInfoUtil {



    public static PhoneCodeVo Get(String mobile){
        PhoneCodeVo codeVo = null;
        Integer num = NumberUtil.getRandomSixNum();//当前的手机号的验证码
        final String APPKEY = "9c79bfbd848d5832";// 你的appkey
        final String URL = "http://api.jisuapi.com/sms/send";
        final String content = "短信验证码"+num+"，请在十分钟内完成验证。【NOTE布克】";// utf-8
        String result = null;
        String count= null;
        String accountid= null;
        String str = null;
        try {
            String url = URL + "?mobile=" + mobile + "&content=" + URLEncoder.encode(content, "utf-8") + "&appkey="
                    + APPKEY;
            result = HttpUtil.sendGet(url, "utf-8");
            JSONObject json = JSONObject.fromObject(result);
            if (json.getInt("status") != 0) {
                System.out.println(json.getString("msg"));
            } else {
                JSONObject resultarr = json.optJSONObject("result");
                count      = resultarr.getString("count");
                accountid = resultarr.getString("accountid");
            }
            if(json.getInt("status") == 210){
                str = "210";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        codeVo = new PhoneCodeVo(count,accountid,num,str);
        return codeVo;
    }

}





