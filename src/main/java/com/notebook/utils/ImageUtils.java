package com.notebook.utils;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.notebook.dto.Dto;
import com.sun.javaws.Globals;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.RenderedImage;
import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/3/30
 */
public class ImageUtils {


    //base64转图片
    public static String GenerateImage(String imgStr,String phone) {
        BASE64Decoder decoder = new BASE64Decoder();
        String imagePath =null;
        try {
            byte[] b = decoder.decodeBuffer(imgStr);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            String path = "/usr/tomcat/webapps/asset/UserHeadImage/";
            String fileName = phone+".jpg";
            imagePath = path+fileName;

            OutputStream out = new FileOutputStream(imagePath);
            out.write(b);out.flush();out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagePath;
    }


}
