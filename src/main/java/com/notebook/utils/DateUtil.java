package com.notebook.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 文件描述:处理日期的一些处理和换算
 * 创建用户:郑凯
 * 创建时间:2018/3/10
 */
public class DateUtil {

    /**
     * 根据传来的Long类型 转换为Date类型
     *
     * @param timestampString 时间戳 如："1473048265";
     * @return String类型  如:"2018-03-10 10:16:23"
     */
    public static String LongToDate(long timestampString) {
        Date date = new Date(timestampString * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
        String value = sdf.format(date);
        return value;
    }

    /**
     * 获取当前时间的时间戳
     *
     * @return 时间戳
     */
    public static long DateToLong() {
        return System.currentTimeMillis() / 1000;

    }

    /**
     * 将字符串的时间格式  转换为date类型
     *
     * @param time 字符串的时间
     * @return
     */
    public static Date StringToDate(String time) {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //加上时间
        try {
            Date date = sDateFormat.parse(time);
            return date;
        } catch (ParseException px) {
            px.printStackTrace();
        }
        return null;
    }


    public static String GetDayLong(int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, day);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String time = sf.format(cal.getTime());
        return time;
    }


    public static String getDateDirName(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        return dateFormat.format(new Date());
    }

    public static String getDateFileName(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("hhmmss");
        return dateFormat.format(new Date());
    }

}
