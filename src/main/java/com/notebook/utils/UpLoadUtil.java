package com.notebook.utils;


import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UpLoadUtil {
    public static Map<String, Object> upLoad(String pathName, HttpServletRequest request, HttpServletResponse response) {
        //创建一个通用的多部分解析器
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        Map<String, Object> map = new HashMap<String, Object>();
        if (multipartResolver.isMultipart(request)) {//判断 request 是否有文件上传,即多部分请求
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request; //转换成多部分request
            Iterator<String> iter = multiRequest.getFileNames(); //取得request中的所有文件名
            List<String> data = new ArrayList<String>();
            while (iter.hasNext()) {
                MultipartFile file = multiRequest.getFile(iter.next());  //取得上传文件
                if (file != null) {
                    String myFileName = file.getOriginalFilename();//取得当前上传文件的文件名称
                    String suffix  = FilenameUtils.getExtension(myFileName);
                    if (myFileName.trim() != "") {  //如果名称不为“”,说明该文件存在，否则说明该文件不存在
                        String fileName = pathName + "."+suffix;//重命名上传后的文件名
                        String appRoot = request.getSession().getServletContext().getRealPath("") + File.separator;//定义上传路径
                        String path = appRoot + fileName;
                        File localFile = new File(path);
                        if (!localFile.exists()) {
                            localFile.mkdirs();
                        }
                        try {
                            file.transferTo(localFile);
                            data.add(fileName);
                        } catch (Exception e) {
                            e.printStackTrace();
                            map.put("errno", 1);
                            map.put("data", data);
                            return map;
                        }
                    }
                }
            }
            map.put("data", data);
            map.put("errno", 0);
        }
        return map;
    }

    public static Map<String, Object> NewsUpLoad(String pathName, HttpServletRequest request, HttpServletResponse response) {
        //创建一个通用的多部分解析器
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        Map<String, Object> map = new HashMap<String, Object>();
        if (multipartResolver.isMultipart(request)) {//判断 request 是否有文件上传,即多部分请求
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request; //转换成多部分request
            Iterator<String> iter = multiRequest.getFileNames(); //取得request中的所有文件名

            String fileName = null;
            while (iter.hasNext()) {
                MultipartFile file = multiRequest.getFile(iter.next());  //取得上传文件
                if (file != null) {
                    String myFileName = file.getOriginalFilename();//取得当前上传文件的文件名称
                    if (myFileName.trim() != "") {  //如果名称不为“”,说明该文件存在，否则说明该文件不存在
                        String prefix = myFileName.substring(myFileName.lastIndexOf(".") + 1);
                        fileName = pathName + DateUtil.getDateFileName() + "." + prefix;  //重命名上传后的文件名
                        String appRoot = request.getSession().getServletContext().getRealPath("") + File.separator;//定义上传路径
                        String path = appRoot + fileName;
                        File localFile = new File(path);
                        if (!localFile.exists()) {
                            localFile.mkdirs();
                        }
                        try {
                            file.transferTo(localFile);

                        } catch (Exception e) {
                            e.printStackTrace();
                            map.put("success", false);

                            return map;
                        }
                    }
                }
            }
            map.put("success", true);
            map.put("url", fileName);
        }
        return map;
    }

    public static  Map<String, Object> UserPic(String pathName, HttpServletRequest request, HttpServletResponse response) {
        //创建一个通用的多部分解析器
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        Map<String, Object> map = new HashMap<String, Object>();
        if (multipartResolver.isMultipart(request)) {//判断 request 是否有文件上传,即多部分请求
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request; //转换成多部分request
            Iterator<String> iter = multiRequest.getFileNames(); //取得request中的所有文件名
            String fileName = null;
            while (iter.hasNext()) {
                MultipartFile file = multiRequest.getFile(iter.next());  //取得上传文件
                if (file != null) {
                    String myFileName = file.getOriginalFilename();//取得当前上传文件的文件名称
                    if (myFileName.trim() != "") {  //如果名称不为“”,说明该文件存在，否则说明该文件不存在
                        String prefix = myFileName.substring(myFileName.lastIndexOf(".") + 1);
                        fileName = pathName + "." + prefix;  //重命名上传后的文件名
                        String appRoot = request.getSession().getServletContext().getRealPath("") + File.separator;//定义上传路径
                        String path = appRoot + fileName;
                        File localFile = new File(path);
                        if (!localFile.exists()) {
                            localFile.mkdirs();
                        }
                        try {
                            file.transferTo(localFile);

                        } catch (Exception e) {
                            e.printStackTrace();


                            return map;
                        }
                    }
                }
            }

            map.put("url", fileName);
        }
        return map;
    }
}
