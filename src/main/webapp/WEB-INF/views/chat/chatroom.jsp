<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" sizes="16x16" href="<%=tBasePath%>/assets/images/favicon.png">
    <title>NOTE布克运维后台</title>

    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">

    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="fix-header card-no-border">
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">应用程序</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">聊天室</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-0">
                    <div class="chat-main-box">
                        <!--left -->
                        <div class="chat-left-aside">
                            <div class="open-panel">
                                <i class="ti-angle-right"></i>
                            </div>
                            <div class="chat-left-inner">
                                <div class="form-material">
                                    <input class="form-control p-20" type="text" placeholder="搜索">
                                </div>
                                <ul class="chatonline style-none ">
                                    <li>
                                        <a href="javascript:void(0)"><img src="<%=tBasePath%>/assets/images/headpic.png"
                                                                          alt="user-img" class="img-circle">
                                            <span>李明强<small class="text-success">在线</small></span></a>
                                    </li>
                                    <li class="p-20"></li>
                                </ul>
                            </div>
                        </div>
                        <!-- right-->
                        <div class="chat-right-aside">
                            <div class="chat-main-header">
                                <div class="p-20 b-b">
                                    <h3 class="box-title">聊天信息</h3>
                                </div>
                            </div>
                            <div class="chat-rbox">
                                <ul class="chat-list p-20">
                                    <li>
                                        <div class="chat-img">
                                            <img src="<%=tBasePath%>/assets/images/headpic.png" alt="user"/>
                                        </div>
                                        <div class="chat-content">
                                            <h5>李明强</h5>
                                            <div class="box bg-light-info">
                                                垃圾李明强
                                            </div>
                                        </div>
                                        <div class="chat-time">17:18:22</div>
                                    </li>
                                    <li class="reverse">
                                        <div class="chat-time">17:18:25</div>
                                        <div class="chat-content">
                                            <h5>郑凯</h5>
                                            <div class="box bg-light-inverse">
                                                你说的对
                                            </div>
                                        </div>
                                        <div class="chat-img">
                                            <img src="<%=tBasePath%>/assets/images/headpic.png" alt="user"/>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="chat-img">
                                            <img src="<%=tBasePath%>/assets/images/headpic.png" alt="user"/>
                                        </div>
                                        <div class="chat-content">
                                            <h5>李明强</h5>
                                            <div class="box bg-light-info">
                                                垃圾李明强
                                            </div>
                                        </div>
                                        <div class="chat-time">17:18:22</div>
                                    </li>
                                    <li class="reverse">
                                        <div class="chat-time">17:18:25</div>
                                        <div class="chat-content">
                                            <h5>郑凯</h5>
                                            <div class="box bg-light-inverse">
                                                你说的对
                                            </div>
                                        </div>
                                        <div class="chat-img">
                                            <img src="<%=tBasePath%>/assets/images/headpic.png" alt="user"/>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body b-t" style="margin-top: -450px;">
                                <div class="row">
                                    <div class="col-8">
                                        <textarea placeholder="Type your message here"
                                                  class="form-control b-0"></textarea>
                                    </div>
                                    <div class="col-4 text-right">
                                        <button type="button" class="btn btn-info btn-circle btn-lg"><i
                                                class="fa fa-paper-plane-o"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-sidebar">
            <div class="slimscrollright">
                <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span></div>
                <div class="r-panel-body">
                    <ul id="themecolors" class="m-t-20">
                        <li><b>With Light sidebar</b></li>
                        <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                        <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                        <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                        <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                        <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                        <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                        <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                        <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                        <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                        <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                        <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                        <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                        <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                    </ul>
                    <ul class="m-t-20 chatonline">
                        <li><b>Chat option</b></li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/1.jpg" alt="user-img"
                                                              class="img-circle"> <span>Varun Dhavan <small
                                    class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/2.jpg" alt="user-img"
                                                              class="img-circle"> <span>Genelia Deshmukh <small
                                    class="text-warning">Away</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/3.jpg" alt="user-img"
                                                              class="img-circle"> <span>Ritesh Deshmukh <small
                                    class="text-danger">Busy</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/4.jpg" alt="user-img"
                                                              class="img-circle"> <span>Arijit Sinh <small
                                    class="text-muted">Offline</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/5.jpg" alt="user-img"
                                                              class="img-circle"> <span>Govinda Star <small
                                    class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/6.jpg" alt="user-img"
                                                              class="img-circle"> <span>John Abraham<small
                                    class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/7.jpg" alt="user-img"
                                                              class="img-circle"> <span>Hritik Roshan<small
                                    class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/8.jpg" alt="user-img"
                                                              class="img-circle"> <span>Pwandeep rajan <small
                                    class="text-success">online</small></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        © 2017 Material Pro Admin by wrappixel.com
    </footer>
    </div>
</div>
<script src="<%=tBasePath%>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=tBasePath%>/assets/js/jquery.slimscroll.js"></script>
<script src="<%=tBasePath%>/assets/js/waves.js"></script>
<script src="<%=tBasePath%>/assets/js/sidebarmenu.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<%=tBasePath%>/assets/js/custom.min.js"></script>
<script src="<%=tBasePath%>/assets/js/chat.js"></script>
<script src="<%=tBasePath%>/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
