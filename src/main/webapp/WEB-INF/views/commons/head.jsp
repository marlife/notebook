<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<div id="main-wrapper">
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!--logo-->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.jsp">
                    <span>
                        <img src="<%=tBasePath%>/assets/images/logo-text.png" alt="homepage" class="dark-logo"/>
                        <img src="<%=tBasePath%>/assets/images/logo-text(1).png" class="light-logo" alt="homepage"/>
                    </span>
                </a>
            </div>
            <!--head right-->
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto mt-md-0">
                    <li class="nav-item">
                        <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
                           href="javascript:void(0)">
                            <i class="mdi mdi-menu"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                           href="javascript:void(0)">
                            <i class="ti-menu"></i>
                        </a>
                    </li>
                    <!-- Search -->
                    <li class="nav-item hidden-sm-down search-box">
                        <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark"
                           href="javascript:void(0)">
                            <i class="ti-search"></i>
                        </a>
                        <form class="app-search">
                            <input type="text" class="form-control" placeholder="搜索你要知道的并按回车键">
                            <a class="srh-btn"><i class="ti-close"></i></a>
                        </form>
                    </li>
                </ul>
                <!-- User profile and search -->
                <ul class="navbar-nav my-lg-0">
                    <!-- Opinion -->
                    <li class="nav-item dropdown">
                        <a  style="margin-top: 10px;" class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-email"></i>
                            <div style="margin-top: 10px;display: none" class="notify">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                        </a>
                        <div class="dropdown-menu mailbox dropdown-menu-right scale-up" aria-labelledby="2">
                            <ul>
                                <li>
                                    <div class="drop-title">你有<span id="count">0</span>条消息</div>
                                </li>
                                <li>
                                    <div class="message-center">

                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="/opinion/list.html">
                                        <strong>查看所有信息</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- adminInfo -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href=""
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="${USER_SESSION.userHeadPic}" alt="user" class="profile-pic"/>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right scale-up">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img">
                                            <img src="${USER_SESSION.userHeadPic}" alt="user">
                                        </div>
                                        <div class="u-text">

                                            <h6>${USER_SESSION.userName}</h6>
                                            <p class="text-muted" style="font-size: 12px;">${USER_SESSION.userEmali}</p>
                                            <a href="/admin/user_${USER_SESSION.userId}.md" class="btn btn-rounded btn-danger btn-sm">个人资料</a>
                                        </div>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="ti-settings"></i> 账号设置</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/login.html"><i class="fa fa-power-off"></i> 退出</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <script src="<%=tBasePath%>/assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        // 实时获取未读的留言数量
        function loadCount() {
            $.post("/opinion/getCount",function (dto) {
                if(dto.success){

                    $("#count").html(dto.data);
                }
            })
            $.post("/opinion/noReads",function (dto) {
                if(dto.data.length!=0){
                    $(".notify").css("display","block")
                }
                for (i=0;i<dto.data.length;i++){
                    $(".message-center").append("  <a href='/opinion/list.html'><div class=\"mail-contnet\">\n" +
                        "                                                <span class=\"mail-desc\">"+dto.data[i].proposalContent+"</span>\n" +
                        "                                                <span class=\"time\">"+getDate(dto.data[i].proposalTime*1000)+"</span>\n" +
                        "                                            </div>\n" +
                        "                                        </a>");

                }
            })
        }



        setInterval(loadCount(),30000);
        function getDate(tm){
            var tt=new Date(tm).toLocaleString().replace(/\//g, "-");
            return tt;
        }
    </script>