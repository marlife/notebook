<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+tPath+"/";
%>
<!-- Bootstrap Core CSS -->
<link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- chartist CSS -->
<link href="<%=tBasePath%>/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
<link href="<%=tBasePath%>/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
<link href="<%=tBasePath%>/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
<!--This page css - Morris CSS -->
<link href="<%=tBasePath%>/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->