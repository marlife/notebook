<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile" style="background: url(<%=tBasePath%>/assets/images/background/user-info.jpg) no-repeat;">
            <div class="profile-img">
                <img src="${USER_SESSION.userHeadPic}" alt="user"/>
            </div>
            <div class="profile-text">
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown"
                   role="button" aria-haspopup="true" aria-expanded="true">${USER_SESSION.userName}</a>
                <div class="dropdown-menu animated flipInY">
                    <a href="/admin/user_${USER_SESSION.userId}.md" class="dropdown-item">
                        <i class="ti-user"></i> 我的简介</a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="ti-settings">
                    </i> 账户设置</a>
                    <div class="dropdown-divider"></div>
                    <a href="/login.html" class="dropdown-item">
                        <i class="fa fa-power-off"></i> 退出</a>
                </div>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">个人的</li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-desktop-mac"></i>
                        <span class="hide-menu">控制面板 </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/index.html">主页面板</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-newspaper"></i>
                        <span class="hide-menu">数据分析</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/news_analysis.html">文章分析</a></li>
                        <li><a href="/user_analysis.html">用户分析</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">用户管理</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/userList">用户列表</a></li>
                        <li><a href="/admin/adminList.md">管理员列表</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-book-open-variant"></i>
                        <span class="hide-menu">文章管理</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/news/newsList.md">文章列表</a></li>
                        <li><a href="/admin/news/news_add.html">编辑文章</a></li>
                        <li><a href="/admin/news/auditlist.md">文章审核</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-account-card-details"></i>
                        <span class="hide-menu">招聘栏目</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/backrecruit/recruits.html">招聘列表</a></li>
                        <li><a href="#">公司列表</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-video"></i>
                        <span class="hide-menu">课堂中心</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/video/list.html">视频管理</a></li>
                        <li><a href="/video/add.html">新增视频</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-bullseye"></i>
                        <span class="hide-menu">应用程序</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/chatroom.html">聊天室</a></li>
                        <li><a href="/chatroom.html">版本中心</a></li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
    <div class="sidebar-footer">
        <a href="/setting.html" class="link" data-toggle="tooltip" title="设置">
            <i class="ti-settings"></i>
        </a>
        <a href="" class="link" data-toggle="tooltip" title="邮箱">
            <i class="mdi mdi-gmail"></i>
        </a>
        <a href="" class="link" data-toggle="tooltip" title="退出">
            <i class="mdi mdi-power"></i>
        </a>
    </div>
</aside>