<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>
    <jsp:include page="../commons/meta.jsp"/>

    <jsp:include page="../commons/css.jsp"/>


</head>
<body class="fix-header fix-sidebar card-no-border">
<jsp:include page="../commons/head.jsp"/>
<!--left menu-->
<jsp:include page="../commons/leftmenu.jsp"/>
<!-- Page wrapper  -->
<div class="page-wrapper">

    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor">控制面板</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">主页面板</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"></h4>
                        </div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="card card-inverse card-primary">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="m-r-20 align-self-center">
                                <h1 class="text-white"><i class="ti-pencil-alt"></i></h1></div>
                            <div>
                                <h3 class="card-title">文章总数</h3>
                                <h6 class="card-subtitle"></h6></div>
                        </div>
                        <div class="row">
                            <div class="col-4 align-self-center">
                                <h5 class="font-light text-white">${newsCount}</h5>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="card card-inverse card-success">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="m-r-20 align-self-center">
                                <h1 class="text-white"><i class="ti-thumb-up "></i></h1></div>
                            <div>
                                <h3 class="card-title">累计点赞</h3>
                                <h6 class="card-subtitle"></h6></div>
                        </div>
                        <div class="row">
                            <div class="col-4 align-self-center">
                                <h5 class="font-light text-white">${newsLikeCount}</h5>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="card card-inverse card-primary">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="m-r-20 align-self-center">
                                <h1 class="text-white"><i class=" ti-eye"></i></h1></div>

                            <div>
                                <h3 class="card-title">累计阅读</h3>
                                <h6 class="card-subtitle"></h6></div>
                        </div>
                        <div class="row">
                            <div class="col-4 align-self-center">
                                <h5 class="font-light text-white">${newsClickNumSum}</h5>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="card card-inverse card-success">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="m-r-20 align-self-center">
                                <h1 class="text-white"><i class="ti-heart"></i></h1></div>
                            <div>
                                <h3 class="card-title">累计收藏</h3>
                                <h6 class="card-subtitle"></h6></div>
                        </div>
                        <div class="row">
                            <div class="col-4 align-self-center">
                                <h5 class="font-light text-white">${favoriteCount}</h5>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">文章概况</h3>
                                    </div>
                                    <div class="ml-auto">
                                        <ul class="list-inline">
                                            <li>
                                                <h6 class="text-muted text-info">
                                                    <i class="fa fa-circle font-10 m-r-10"></i>文章数</h6>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="ct-bar-chart" style="height: 381px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../commons/right.jsp"/>
    </div>
    <jsp:include page="../commons/foot.jsp"/>
    <script src="<%=tBasePath%>/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<%=tBasePath%>/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <script type="text/javascript">
        //获取AddDayCount天后的日期//获取当前月份的日期
        function GetDate(AddDayCount){
            var dd = new Date();
            dd.setDate(dd.getDate()+AddDayCount);
            var y = dd.getFullYear();
            var m = dd.getMonth()+1;
            var d = dd.getDate();
            return y+"-"+m+"-"+d;
        }
        var data = {
            labels: [GetDate(-7),GetDate(-6),GetDate(-5),GetDate(-4),GetDate(-3),GetDate(-2),GetDate(-1)],
            series: [

                [${counts.get(6)},${counts.get(5)},${counts.get(4)},${counts.get(3)},${counts.get(2)},${counts.get(1)},${counts.get(0)}]
            ]
        };
        var options = {
            seriesBarDistance: 10
        };
        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 8,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];
        new Chartist.Bar('.ct-bar-chart', data, options, responsiveOptions);


    </script>
</body>
</html>