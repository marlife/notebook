<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html>
<head>
    <jsp:include page="../commons/meta.jsp"/>
    <link rel="stylesheet" href="<%=tBasePath%>assets/login/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=tBasePath%>assets/login/css/style.css">
</head>
<body>
<div class="register-container container">
    <div class="row">
        <div class="iphone span5">
            <img src="<%=tBasePath%>assets/login/img/iphone.png" alt="">
        </div>
        <div class="register span6">
            <form action="/admin/login.md" method="post">
                <h2>NOTE布克后台登陆</h2>
                <label for="username">账号：<span class="red">${numError}</span></label>
                <input type="text" value="${phone}" id="username" name="username" placeholder="输入你的账号...">
                <label for="password">密码：<span class="red">${pwdError}</span></label>
                <input type="password" id="password" name="password" placeholder="输入你的密码...">
                <button type="submit">登 陆</button>
            </form>
        </div>
    </div>
</div>


<script src="<%=tBasePath%>assets/login/js/jquery-1.8.2.min.js"></script>
<script src="<%=tBasePath%>assets/login/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=tBasePath%>assets/login/js/jquery.backstretch.min.js"></script>
<script>
    $(document).ready(function () {


        $('.register form').submit(function () {
            var username = $(this).find('input#username').val();
            var password = $(this).find('input#password').val();
            if (username == '') {
                $(this).find("label[for='username']").append("<span style='display:none' class='red'> - 请输入账号</span>");

                return false;
            }
            if (password == '') {
                $(this).find("label[for='password']").append("<span style='display:none' class='red'> - 请输入密码</span>");

                return false;
            }
        });
    });
</script>
</body>

</html>