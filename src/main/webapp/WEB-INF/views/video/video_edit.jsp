<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>

    <title>NOTE布克运维后台</title>
    <jsp:include page="../commons/meta.jsp"/>
    <link rel="stylesheet" type="text/css" href="<%=tBasePath%>/assets/upload/css/default.css">
    <link href="<%=tBasePath%>/assets/upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />

    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">

    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
</head>
<body class="fix-header card-no-border" >
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">课堂中心</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">新增视频</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white">新增视频</h4>
                    </div>
                    <div class="card-body">
                        <form action="/video/add.md" method="post" enctype="multipart/form-data">
                            <div class="form-body">
                                <h3 class="card-title">视频信息</h3>
                                <h3 class="card-title">${info}</h3>
                                <hr>
                                <input type="hidden" name="videoUserId"  value="${USER_SESSION.userId}"/>
                                <div class="row p-t-20">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">视频标题</label>
                                            <input type="text" name="videoTitle"  id="newsTitle"
                                                   class="form-control"
                                                   placeholder="好的视频标题能让用户快速的记住">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!--type-->
                                    <div class="col-md-12">
                                        <div class="form-group has-success">
                                            <label class="control-label">视频分类</label>
                                            <select  name="videoType" class="form-control custom-select">
                                                <c:forEach var="item" items="${types}">
                                                    <option value="${item.newsTypeId}">${item.newsTypeName}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>视频封面图</label>
                                            <input id="file-3" name="cover" type="file" multiple=true/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <input type="hidden" name="videoDetails" />
                                            <label>视频详情</label>
                                            <div id="editor">
                                                ${news.newsContent}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="tijiao btn btn-success"><i class="fa fa-check"></i>提交
                                </button>
                                <button type="button" class="btn btn-inverse">重填</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../commons/right.jsp"/>
    </div>
    <jsp:include page="../commons/foot.jsp"/>

    <script src="<%=tBasePath%>/assets/upload/js/fileinput.js" type="text/javascript"></script>
    <!--简体中文-->
    <script src="<%=tBasePath%>/assets/upload/js/locales/zh.js" type="text/javascript"></script>
    <!--繁体中文-->
    <script src="<%=tBasePath%>/assets/upload/js/locales/zh-TW.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=tBasePath%>/assets/plugins/wangEditor/wangEditor.js"></script>

    <script>
        $("#file-3").fileinput({
            showUpload: false,
            showCaption: false,
            browseClass: "btn btn-primary btn-lg",
            fileType: "any",
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
        });
        var E = window.wangEditor
        var editor = new E('#editor');
        editor.customConfig.uploadImgServer = '/video/upload';
        editor.customConfig.uploadImgHooks = {
            customInsert: function (insertImg, result, editor) {
                var url = result.data;
                insertImg(url);
            }
        };
        editor.create();




        $(function () {
            $("form").submit(function () {
                $("[name=videoDetails]").val(editor.txt.html());

                return true;

            });
        });

    </script>
</body>
</html>
