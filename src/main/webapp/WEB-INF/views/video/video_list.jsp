<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>
    <jsp:include page="../commons/meta.jsp"/>
    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">

</head>
<body class="fix-header fix-sidebar card-no-border">
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">视频管理</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">视频详情列表</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row el-element-overlay">
            <c:forEach var="item" items="${videoList}">
                <div class="col-lg-3 col-md-6" style="height: 200px;">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <img src="${item.videoCover}" alt="user"/>
                                <div class="el-overlay">
                                    <ul class="el-info">
                                        <li>
                                            <a class="btn default btn-outline" title="查看"
                                               onclick="selectChapter(${item.videoId})" data-toggle="modal"
                                               data-target="#add-contact">
                                                <i class="icon-control-play"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="btn default btn-outline" title="删除"
                                               href="/video/del/${item.videoId}.html">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="el-card-content">
                                <h5 class="box-title">${item.videoTitle}</h5>
                                <jsp:setProperty name="dateValue" property="time" value="${item.videoTime*1000}"/>
                                <small><fmt:formatDate value="${dateValue}" pattern="yyyy-MM-dd HH:mm"/></small>
                                <small>${item.newsType.newsTypeName}</small>
                                <br/></div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div id="add-contact"  class="modal fade in" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog"  >
                <div class="modal-content" style="margin-top:10%;max-height: 70%;overflow-y: scroll;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">×
                        </button>
                        <h4 class="modal-title" id="myModalLabel">视频目录</h4></div>
                    <div class="modal-body">
                        <div class="row" id="chapterInfo">

                        </div>
                            <div class="modal-footer">
                                <button type="button" class="btn tijiao btn-info " data-toggle="modal"
                                        data-target="#add-chapter">
                                    新增
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div  id="add-chapter" class="modal fade in" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×
                            </button>
                            <h6 class="modal-title" id="add_chapter">新增视频目录</h6></div>
                        <div class="modal-body">
                            <form class="m-t-40" action="/chapter/add.html" method="post" novalidate>
                                <input type="hidden" name="videoId"/>
                                <div class="form-group">
                                    <h5>视频标题<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="chapterTitle" class="form-control"></div>

                                </div>
                                <div class="form-group">
                                    <h5>视频地址<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="videoUrl" class="form-control"></div>

                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn tijiao btn-info ">保存
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <jsp:include page="../commons/right.jsp"/>

        </div>
        <jsp:include page="../commons/foot.jsp"/>
        <script src="../assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
        <script src="../assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
        <script>
            function selectChapter(id) {

                $("[name=videoId]").val(id);
                $(".chapter").remove();
                $.post("/chapter/info/" + id + ".md", function (dto) {
                    if (dto.success) {
                        for (i = 0; i < dto.data.length; i++) {
                            $("#chapterInfo").append("<div class=\"col-12 chapter\">\n" +
                                "                                <div class=\"card\">\n" +
                                "                                    <div class=\"card-body\">\n" +
                                "                                        <a  target='_blank' href=\""+dto.data[i].videoUrl+"\"style=\"color: #263238\">\n" +
                                "                                        <span>\n" +
                                "                                            <i class=\"icon-control-play\"></i> \n" +dto.data[i].chapterTitle+
                                "                                            <a href=\"/chapter/del/"+dto.data[i].chapterId+".html\" style=\"float: right\"><i class=\"icon-trash\"></i></a>\n" +
                                "                                        </span>\n" +
                                "                                        </a>\n" +
                                "                                    </div>\n" +
                                "                                </div>\n" +
                                "                            </div>");
                        }
                    }
                })
            }
        </script>
</body>
</html>
