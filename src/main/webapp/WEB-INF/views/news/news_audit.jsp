<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>

<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">

<head>
    <jsp:include page="../commons/meta.jsp"/>
    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/plugins/footable/css/footable.core.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet"/>
    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
</head>

<body class="fix-header card-no-border">
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">文章审核</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">文章审核</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">待审核文章详情</h4>
                        <table id="demo-foo-addrow2" class="table table-bordered table-hover toggle-circle"
                               data-page-size="7">
                            <thead>
                            <tr>
                                <th data-sort-initial="true" data-toggle="true">文章标题</th>
                                <th>文章分类</th>
                                <th data-hide="phone, tablet">点击次数</th>
                                <th data-hide="phone, tablet">发布人</th>
                                <th data-hide="phone, tablet">发布时间</th>
                                <th data-hide="phone, tablet">文章状态</th>
                                <th data-sort-ignore="true" class="min-width">操作</th>
                            </tr>
                            </thead>
                            <div class="m-t-40">
                                <div class="d-flex">

                                    <div class="ml-auto">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="搜索"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tbody>
                            <c:forEach var="item" items="${userNews}">
                                <tr>

                                    <td>
                                        <input type="hidden" id="nid" value="${item.newsId}"/>
                                        <input type="hidden" id="uid" value="${item.newsUserId}"/>
                                        <a href="/admin/news/${item.newsId}.md" class="link" data-toggle="tooltip"
                                           title="${item.newsTitle}">
                                                ${item.newsTitle}</a>
                                    </td>
                                    <td>${item.type.newsTypeName}</td>
                                    <td>${item.clickNumber}次</td>
                                    <td>
                                        <a href="/admin/user_${item.newsUserId}.md" class="link" data-toggle="tooltip"
                                           title="${item.showUser.userName}">
                                                ${item.userName}</a>
                                            <jsp:setProperty name="dateValue" property="time"
                                                             value="${item.createtime*1000}"/>
                                    <td><fmt:formatDate value="${dateValue}" pattern="MM/dd/yyyy HH:mm"/></td>
                                    <td>
                                        <c:if test="${item.isRelease==-1}">
                                            <span class="label label-table label-warning">审核未通过</span>
                                        </c:if>
                                        <c:if test="${item.isRelease==0}">
                                            <span class="label label-table label-primary">待审核</span>
                                        </c:if>
                                    </td>
                                    <td>
                                        <button type="button" style="height: 30px;" id="audit" class="btn btn-info "
                                                data-toggle="modal"
                                                data-target="#add-contact" onclick="fuzhi(this)">审核文章
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                            <tfoot>
                            <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">×
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">审核文章</h4></div>
                                        <div class="modal-body">
                                            <form action="/admin/news/audit.md" id="form1" method="post">
                                                <input type="hidden" name="nid" id="newsId"/>
                                                <input type="hidden" name="uid" id="userId"/>
                                                <input type="hidden" name="isrelease" id="isrelease"/>
                                                <div class="form-body">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">审核标题</label>
                                                            <input type="text" name="messageTitle" id="firstName" class="form-control"
                                                                   placeholder="消息的标题">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>消息内容</label>
                                                            <textarea name="messageText" class="form-control" rows="5"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" id="yesNews" class="btn btn-success ">审核通过</button>
                                                    <button type="button" id="noNews" class="btn btn-warning">审核不通过</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tr>
                                <td colspan="6">
                                    <div class="text-right">
                                        <ul class="pagination">
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="../commons/right.jsp"/>

    </div>
    <footer class="footer"> 2018 © NOTE布克 www.notebuke.cn | 黑ICP备17009246号-2</footer>
</div>
</div>
<script src="<%=tBasePath%>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=tBasePath%>/assets/js/jquery.slimscroll.js"></script>
<script src="<%=tBasePath%>/assets/js/waves.js"></script>
<script src="<%=tBasePath%>/assets/js/sidebarmenu.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<%=tBasePath%>/assets/js/custom.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/footable/js/footable.all.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<%=tBasePath%>/assets/js/footable-init.js"></script>
<script src="<%=tBasePath%>/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<script>
    function fuzhi(obj) {
        var n = $(obj).parents("tr").children().eq(0).children("input").eq(0).val();
        var u = $(obj).parents("tr").children().eq(0).children("input").eq(1).val();
        $("#newsId").val(n);
        $("#userId").val(u);

    };
    $(function () {

       $("#yesNews").click(function () {
           $("#isrelease").val("1");
            $("#form1").submit();
       });
        $("#noNews").click(function () {
            $("#isrelease").val("-1");
           $("#form1").submit();

        });

    });
</script>
</body>
</html>
