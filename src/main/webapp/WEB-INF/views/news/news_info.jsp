<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>
    <jsp:include page="../commons/meta.jsp"/>
    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

</head>
<body class="fix-header card-no-border">
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">文章详情</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">文章内容查看</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        ${newsContent}
                    </div>
                </div>
                <h3>评论列表</h3>
                <div class="row" style="max-height: 600px; overflow-y: auto">
                    <c:forEach var="item" items="${newsComms}">
                        <div id="commId" class="col-lg-12 col-md-12 col-xlg-12 col-xs-12 ">
                            <div class="ribbon-wrapper card">
                                <div class="ribbon ribbon-success">
                                    <img src="${item.newsCommUserVO.commUserPic}" width="30" height="30"
                                         class="img-circle">
                                    <span>${item.newsCommUserVO.commUserName}</span>
                                    <jsp:setProperty name="dateValue" property="time" value="${item.commTime*1000}"/>
                                    <small><fmt:formatDate value="${dateValue}" pattern="MM/dd/ HH:mm:ss"/></small>
                                </div>
                                <div class="ribbon-content">
                                        ${item.commContent}
                                <span style="position: absolute;right: 5px;">
                                     <a onclick="delComm(this,${item.commId})" href="javascript:;">删除</a>
                                </span>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>


        <jsp:include page="../commons/right.jsp"/>
        <jsp:include page="../commons/foot.jsp"/>
        <script type="text/javascript">
            function delComm(obj,id) {
                $.post("/newsComm/delete/"+id,function (dto) {
                        if(dto.success){
                            $(obj).parents("#commId").remove()
                        }
                })
            }
        </script>
</body>
</html>
