<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>

    <title>NOTE布克运维后台</title>
    <jsp:include page="../commons/meta.jsp"/>
    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">

    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">

</head>
<body class="fix-header card-no-border" >
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">新增文章</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">文章管理</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white">新增文章</h4>
                    </div>
                    <div class="card-body">
                        <form action="/admin/news/editor" method="post">
                            <input type="hidden" value="${news.newsId}" name="newsId"/>
                            <div class="form-body">
                                <h3 class="card-title">文章信息</h3>
                                <hr>
                                <input type="hidden" name="userId" value="${USER_SESSION.userId}"/>
                                <div class="row p-t-20">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">文章标题</label>
                                            <input type="text" value="${news.newsTitle}" name="newsTitle" id="newsTitle"
                                                   class="form-control"
                                                   placeholder="好的文章标题能让用户快速的记住">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!--type-->
                                    <div class="col-md-12">
                                        <div class="form-group has-success">
                                            <label class="control-label">文章分类</label>
                                            <select  name="newsTypeId" class="form-control custom-select">
                                                <c:forEach var="item" items="${types}">
                                                    <option value="${item.newsTypeId}"<c:if test="${news.newsTypeId==item.newsTypeId}">selected</c:if>>${item.newsTypeName}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="key">关键字</label>
                                            <input type="text" value="${news.newsKey}" name="newsKey" id="key"
                                                   class="form-control"
                                                   placeholder="好的关键字能让用户快速的找到想要的东西，多个关键字用空格分开">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <input type="hidden" name="newsContent"/>
                                            <label>文章内容</label>
                                            <div id="editor">
                                                ${news.newsContent}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="tijiao btn btn-success"><i class="fa fa-check"></i>提交
                                </button>
                                <button type="button" class="btn btn-inverse">重填</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../commons/right.jsp"/>
    </div>
    <jsp:include page="../commons/foot.jsp"/>
    <script type="text/javascript" src="<%=tBasePath%>/assets/plugins/wangEditor/wangEditor.js"></script>
    <script>

        var E = window.wangEditor
        var editor = new E('#editor');
        editor.customConfig.uploadImgServer = '/news/upload?type=news';
        editor.customConfig.uploadImgHooks = {
            customInsert: function (insertImg, result, editor) {
                var url = result.data;
                insertImg(url);
            }
        };
        editor.create();




        $(function () {
            $("form").submit(function () {
                $("[name=newsContent]").val(editor.txt.html());
                return true;

            });
        });
    </script>
</body>
</html>
