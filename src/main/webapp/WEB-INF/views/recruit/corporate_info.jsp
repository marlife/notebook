<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">

<head>
    <jsp:include page="../commons/meta.jsp"/>
    <title>NOTE布克运维后台</title>
    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</head>
<body class="fix-header card-no-border">
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">个人简介</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">个人信息</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30">
                            <img src="${user.user_head_pic}" class="img-circle" width="150"/>
                        </center>

                            <div class="row text-center justify-content-md-center">
                                <div class="col-4">
                                    <small class="text-muted">公司主页</small>
                                    <h6 ><a  target="_blank"  class="link" href="http://${corporate.corporateHomepage}">http://${corporate.corporateHomepage}</a></h6>
                                    <small class="text-muted p-t-30 db">公司地址</small>
                                    <h6>${corporate.corporateAddress}</h6>
                                </div>
                                <div class="col-4">
                                    <h3 class="font-medium">${corporate.corporateName}</h3>
                                </div>
                                <div class="col-4">
                                    <small class="text-muted">邮箱</small>
                                    <h6>${user.user_emali}</h6>
                                    <small class="text-muted p-t-30 db">电话</small>
                                    <h6>${user.user_phone}</h6>
                                </div>
                             </div>

                    </div>
                    <div>
                        <hr>
                    </div>
                    <div class="card-body">
                        <h6>公司描述</h6>
                        <p>
                            ${corporate.corporateDescription}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../commons/right.jsp"/>
    </div>
    <jsp:include page="../commons/foot.jsp"/>
</body>
</html>
