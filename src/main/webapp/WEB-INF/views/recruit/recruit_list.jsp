<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>
    <jsp:include page="../commons/meta.jsp"/>
    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/plugins/footable/css/footable.core.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet"/>
    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
</head>
<body class="fix-header card-no-border">
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">招聘列表详情</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">招聘列表</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">招聘列表详情</h4>
                        <table id="demo-foo-addrow2" class="table table-bordered table-hover toggle-circle"
                               data-page-size="5">
                            <thead>
                            <tr>
                                <th data-sort-initial="true" data-toggle="true">公司名称</th>
                                <th>岗位名称</th>
                                <th data-hide="phone, tablet">发布人</th>
                                <th data-hide="phone, tablet">发布时间</th>
                                <th data-sort-ignore="true" class="min-width">操作</th>
                            </tr>
                            </thead>
                            <div class="m-t-40">
                                <div class="d-flex">
                                    <div class="ml-auto">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="搜索"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tbody>
                            <c:forEach items="${Recruits}" var="item">
                                <tr>
                                    <td>
                                        <a href="/corporate/${item.corporate_id}.html" class="link" data-toggle="tooltip" title="${item.corporate_name}">
                                                ${item.corporate_name}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/backrecruit/${item.recruit_id}.html" class="link" data-toggle="tooltip" title="${item.post_name}">
                                                ${item.post_name}
                                        </a>
                                    </td>
                                    <td>
                                        <a class="link" href="/admin/user_${item.user_id}.md">
                                            <c:if test="${item.user_head_pic!=null}">
                                                <img src="${item.user_head_pic}" alt="user"
                                                     width="40" class="img-circle"/>
                                            </c:if>
                                            <c:if test="${item.user_head_pic==null}">
                                                <img src="http://140.143.57.236:8080/asset/UserHeadImage/admin.png"
                                                     alt="user"
                                                     width="30" class="img-circle"/>
                                            </c:if>
                                                ${item.user_name}
                                        </a>
                                    </td>
                                    <jsp:setProperty name="dateValue" property="time" value="${item.createtime*1000}"/>

                                    <td><fmt:formatDate value="${dateValue}" pattern="MM/dd/yyyy HH:mm"/></td>

                                    <td>
                                        <button type="button"
                                                class="btn btn-sm btn-icon btn-pure btn-outline"
                                                data-toggle="tooltip" data-original-title="删除">
                                            <i class="ti-close"  aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="text-right">
                                        <ul class="pagination">
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="../commons/right.jsp"/>

    </div>
    <footer class="footer"> 2018 © NOTE布克 www.notebuke.cn | 黑ICP备17009246号-2</footer>
</div>
</div>
<script src="<%=tBasePath%>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=tBasePath%>/assets/js/jquery.slimscroll.js"></script>
<script src="<%=tBasePath%>/assets/js/waves.js"></script>
<script src="<%=tBasePath%>/assets/js/sidebarmenu.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<%=tBasePath%>/assets/js/custom.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/footable/js/footable.all.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<%=tBasePath%>/assets/js/footable-init.js"></script>
<script src="<%=tBasePath%>/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

</body>
</html>
