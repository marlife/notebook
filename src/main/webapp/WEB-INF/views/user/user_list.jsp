<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>
<c:if test="${empty sessionScope.USER_SESSION}">
    <c:redirect url="/login.html"/>
</c:if>
<%
    String tPath = request.getContextPath();
    String tBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + tPath + "/";
%>
<html lang="en">
<head>
    <jsp:include page="../commons/meta.jsp"/>
    <title>NOTE布克运维后台</title>
    <link href="<%=tBasePath%>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/plugins/footable/css/footable.core.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet"/>
    <link href="<%=tBasePath%>/assets/css/style.css" rel="stylesheet">
    <link href="<%=tBasePath%>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
</head>
<body class="fix-header card-no-border">
<jsp:include page="../commons/head.jsp"/>
<jsp:include page="../commons/leftmenu.jsp"/>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">用户列表</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">主页</a></li>
                    <li class="breadcrumb-item active">用户列表</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                        <div class="chart-text m-r-10">

                            <h4 class="m-t-0 text-info"><a href="javascript:history.back(-1)">返回</a></h4></div>

                    </div>

                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10">
                            <i class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">用户信息列表</h4>
                        <h6 class="card-subtitle"></h6>
                        <div class="table-responsive">
                            <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list"
                                   data-page-size="10">
                                <thead>
                                <tr>
                                    <th>编号</th>
                                    <th>名称</th>
                                    <th>邮箱</th>
                                    <th>电话</th>

                                    <th>注册时间</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="item" items="${userList}">
                                    <tr>


                                        <td>${item.userId}</td>
                                        <td>
                                            <a href="/admin/user_${item.userId}.md">
                                                <c:if test="${item.userHeadPic!=null}">
                                                    <img src="${item.userHeadPic}" alt="user"
                                                         width="40" class="img-circle"/>
                                                </c:if>
                                                <c:if test="${item.userHeadPic==null}">
                                                    <img src="http://140.143.57.236:8080/asset/UserHeadImage/admin.png"
                                                         alt="user"
                                                         width="40" class="img-circle"/>
                                                </c:if>
                                                    ${item.userName}
                                            </a>
                                        </td>
                                        <td>${item.userEmali}</td>
                                        <td>${item.userPhone}</td>
                                        <jsp:setProperty name="dateValue" property="time" value="${item.regTime*1000}"/>

                                        <td><fmt:formatDate value="${dateValue}" pattern="MM/dd/yyyy HH:mm"/></td>
                                        <td class="td-status">
                                            <c:if test="${item.accountStatus==true}">
                                                <span class="label label-success">已启用</span>
                                            </c:if>
                                            <c:if test="${item.accountStatus==false}">
                                                <span class="label label-primary">已锁定</span>
                                            </c:if>
                                        </td>
                                        <td class="td-manage">


                                            <button type="button" onclick="member_del(this,${item.userId})"
                                                    class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn"
                                                    data-toggle="tooltip" data-original-title="删除">
                                                <i class="ti-close" aria-hidden="true"></i></button>


                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2">
                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal"
                                                data-target="#add-contact">新增用户
                                        </button>
                                    </td>
                                    <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">×
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">新增用户</h4></div>
                                                <div class="modal-body">
                                                    <form class="m-t-40" action="/admin/addUser" method="post"  novalidate>
                                                        <input type="hidden" name="userPower" value="false"/>
                                                        <div class="form-group">
                                                            <h5>用户名<span class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="userName" class="form-control"
                                                                       required
                                                                       data-validation-required-message="请输入你的名称"></div>
                                                            <div class="form-control-feedback"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <h5>邮箱地址<span class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="text" name="userEmail" class="form-control"
                                                                       data-validation-regex-regex="([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})"
                                                                       data-validation-regex-message="请输入正确的邮箱地址"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <h5>手机号<span class="text-danger phone">*</span></h5>
                                                            <div class="controls">
                                                                <input onblur="yanzheng()" type="text" name="phone" class="form-control"
                                                                       required data-validation-containsnumber-regex="(\d)+"
                                                                       data-validation-containsnumber-message="请输入正确的手机号">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <h5>密码<span class="text-danger">*</span>
                                                            </h5>
                                                            <div class="controls">
                                                                <input type="password" name="password"
                                                                       class="form-control" required
                                                                       data-validation-required-message="请输入密码">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn tijiao btn-info "
                                                                    >保存
                                                            </button>
                                                            <button type="button" class="btn btn-default"
                                                                    >充填
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <td colspan="7">
                                        <div class="text-right">
                                            <ul class="pagination"></ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../commons/right.jsp"/>
    </div>
    <footer class="footer"> © 2017 Material Pro Admin by wrappixel.com</footer>
</div>

</div>
<script src="<%=tBasePath%>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=tBasePath%>/assets/js/jquery.slimscroll.js"></script>
<script src="<%=tBasePath%>/assets/js/waves.js"></script>
<script src="<%=tBasePath%>/assets/js/sidebarmenu.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<%=tBasePath%>/assets/js/custom.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/footable/js/footable.all.min.js"></script>
<script src="<%=tBasePath%>/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<%=tBasePath%>/assets/js/footable-init.js"></script>
<script src="<%=tBasePath%>/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

<script>
    /*用户-删除*/
    function member_del(obj, id) {
        $.ajax({
            type: 'DELETE',
            url: '/admin/user/' + id,
            dataType: 'json',
            success: function (dto) {
                if (dto.success) {
                    $(obj).parents("tr").remove();

                }
            },
        });
    }


        !function (window, document, $) {
            "use strict";
            $("input").not("[type=submit]").jqBootstrapValidation()

        }(window, document, jQuery);


</script>

</body>
</html>
