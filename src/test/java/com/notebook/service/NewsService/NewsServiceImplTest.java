package com.notebook.service.NewsService;

import com.notebook.entity.vo.news.NewsVo;
import com.notebook.service.NewsCommService.INewsCommService;
import com.notebook.utils.DateUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/5/25
 */
public class NewsServiceImplTest {

    @Autowired
    private INewsCommService newsCommService;
    @Autowired
    private INewsService newsService;
    public NewsServiceImplTest(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
        newsService = applicationContext.getBean(INewsService.class);
        newsCommService = applicationContext.getBean(INewsCommService.class);
    }
    @Test
    public void selectByPrimaryKey() {
        System.out.println(newsService.selectByPrimaryKey(199));
    }

    @Test
    public void updateByPrimaryKey() {
        NewsVo newsVo = new NewsVo(225, "hahhahahah","哈哈哈哈哈哈哈","<p>哈哈哈哈哈哈</p>",7);
        System.out.println(newsService.backUpdateNews(newsVo));
    }
    @Test
    public void deleteId(){
        System.out.println(newsCommService.deleteById(189));
    }

    @Test
    public void homeNewsList() {
        System.out.println(newsService.HomeNewsList());
    }

    @Test
    public void getTodayNewsUserInfo() {
        System.out.println(newsService.getTodayNewsUserInfo());
    }

    @Test
    public void getQuarterNewsUserInfo() {
        System.out.println(newsService.getQuarterNewsUserInfo());
    }

    @Test
    public void getWeekNewsUserInfo() {
        System.out.println(newsService.getWeekNewsUserInfo());
    }

    @Test
    public void getYearNewsUserInfo() {
        System.out.println(newsService.getYearNewsUserInfo());
    }

    @Test
    public void getMonthNewsUserInfo() {
        System.out.println(newsService.getMonthNewsUserInfo());
    }

    @Test
    public void getNewsDayCount() {
        System.out.println(newsService.GetNewsDayCount());
    }

    @Test
    public void getLastNewsInfo() {
        System.out.println(newsService.getLastNewsInfo());
    }
}