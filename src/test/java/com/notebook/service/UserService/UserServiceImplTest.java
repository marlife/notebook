package com.notebook.service.UserService;

import com.notebook.entity.vo.user.AddUserVO;
import com.notebook.service.NewsService.INewsService;
import com.notebook.utils.DateUtil;
import com.notebook.utils.EncryptUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/5/25
 */
public class UserServiceImplTest {
    @Autowired
    private IUserService userService;
    public UserServiceImplTest(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
        userService = applicationContext.getBean(IUserService.class);
    }
    @Test
    public void userList() {
        System.out.println(userService.userList());
    }

    @Test
    public void updateUserInfo() {
        Map<String,Object> map = new HashMap<String,Object>(0);




        map.put("status",0);
        map.put("power",0);
        map.put("userId",83);

        System.out.println(userService.updateUserInfo(map));
    }

    @Test
    public void addUserInfo() {
        AddUserVO addUserVO = new AddUserVO();
        addUserVO.setUserName("测试");
        addUserVO.setPhone("12315153153");
        addUserVO.setPassword(EncryptUtil.md5Util("aa2132"));
        addUserVO.setRegTime(DateUtil.DateToLong());
        addUserVO.setUserEmail("测试");
        addUserVO.setUserPower(true);
        System.out.println(userService.addUserInfo(addUserVO));
    }

    @Test
    public void selectByPrimaryKey() {
        System.out.println(userService.selectByPrimaryKey(17));
    }

    @Test
    public void getCorporateUserInfo() {
        System.out.println(userService.getCorporateUserInfo(53));
    }
}