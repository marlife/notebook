package com.notebook.service.NewsTypeService;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/5/28
 */
public class NewsTypeServiceImplTest {

    @Autowired
    private INewsTypeService newsTypeService;
    public NewsTypeServiceImplTest(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
        newsTypeService = applicationContext.getBean(INewsTypeService.class);
    }
    @Test
    public void queryAll() {
        System.out.println(newsTypeService.queryAll());
    }
}