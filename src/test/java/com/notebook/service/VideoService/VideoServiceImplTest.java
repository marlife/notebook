package com.notebook.service.VideoService;

import com.notebook.dao.IVideoMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/6/5
 */
public class VideoServiceImplTest {

    @Autowired
    private IVideoService videoService;
    public VideoServiceImplTest(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
        videoService  = applicationContext.getBean(IVideoService.class);
    }
    @Test
    public void getTheLeastWatch() {
        System.out.println(videoService.getTheLeastWatch());
    }
}