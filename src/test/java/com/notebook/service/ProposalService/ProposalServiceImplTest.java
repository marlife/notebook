package com.notebook.service.ProposalService;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

/**
 * 文件描述:
 * 创建用户:emotion
 * 创建时间:2018/5/28
 */
public class ProposalServiceImplTest {

    @Autowired
    private IProposalService proposalService;

    public ProposalServiceImplTest(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
        proposalService = applicationContext.getBean(IProposalService.class);
    }
    @Test
    public void queryAll() {
        System.out.println(proposalService.queryAll());
    }

    @Test
    public void getNoReadList() {
        System.out.println(proposalService.getNoReadList());
    }
}